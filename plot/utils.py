import os
import math
import sys

import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as ticker
import matplotlib.font_manager as font_manager
import seaborn as sns
import ast
import os
import re
import json as json
import random
import string

from collections import defaultdict
from glob import glob
from six import iteritems
from matplotlib.lines import Line2D
from statsmodels.distributions.empirical_distribution import ECDF
from matplotlib.ticker import FormatStrFormatter

colors = {'red': '#e41a1c', 'blue': '#377eb8',
          'green': '#4daf4a', 'grey': '#404040'}

font = 'Clear Sans'

plt.rcParams["figure.figsize"] = [8.5, 4.5]

plt.rcParams['font.family'] = font
plt.rcParams['font.sans-serif'] = font

plt.style.use('fivethirtyeight')

plt.rcParams['axes.linewidth'] = 1

plt.rcParams['axes.spines.right'] = False
plt.rcParams['axes.spines.top'] = False

plt.rcParams['grid.linestyle'] = '--'

plt.rcParams['ytick.color'] = '#333333'
plt.rcParams['xtick.color'] = '#333333'

plt.rcParams['xtick.direction'] = 'in'
plt.rcParams['ytick.direction'] = 'in'

plt.rcParams['axes.edgecolor'] = '#333333'

plt.rcParams['axes.facecolor'] = 'white'
plt.rcParams['savefig.facecolor'] = 'white'
plt.rcParams['figure.facecolor'] = 'white'

plt.rcParams['xtick.major.size'] = 15
plt.rcParams['xtick.minor.size'] = 8
plt.rcParams['ytick.major.size'] = 15
plt.rcParams['ytick.minor.size'] = 8

plt.rcParams['xtick.major.pad'] = 5
plt.rcParams['ytick.major.pad'] = 5

plt.rcParams['axes.grid.which'] = 'major'

plt.rcParams['font.size'] = 12

plt.rcParams['lines.linewidth'] = 4

plt.rcParams['xtick.labelsize'] = 12
plt.rcParams['ytick.labelsize'] = 12

pd.set_option('precision', 8)

plt.rcParams['text.latex.preamble'] = r'\usepackage{amsmath,amssymb,bm,fontenc,physics,lmodern,nicefrac}'
plt.rcParams["lines.markeredgewidth"] = 2

all_benchmarks = ['get-time-python',
                 'sentiment-analysis-python',
                 'json-python',
                 'markdown-to-html-python',
                 'base64-python',
                 'primes-python',
                 'get-time-node',
                 'autocomplete-node',
                 'json-node',
                 'primes-node',
                 'img-resize-node',
                 'base64-node',
                 'ocr-img-node',
                 "pyperf-chaos-python",
                 "pyperf-logging-python",
                 "pyperf-pyaes-python",
                 "pyperf-spectral_norm-python",
                 "pyperf-deltablue-python",
                 "pyperf-go-python",
                 "pyperf-mdp-python",
                 "pyperf-pyflate-python",
                 "pyperf-telco-python",
                 "pyperf-hexiom-python",
                 "pyperf-nbody-python",
                 "pyperf-raytrace-python",
                 "pyperf-unpack_sequence-python",
                 "pyperf-fannkuch-python",
                 "pyperf-json_dumps-python",
                 "pyperf-pickle-python",
                 "pyperf-richards-python",
                 "pyperf-version-python",
                 "pyperf-float-python",
                 "pyperf-json_loads-python",
                 "pyperf-pidigits-python",
                 "pyperf-scimark-python",
                 "polybench-2mm-c",
                 "polybench-3mm-c",
                 "polybench-adi-c",
                 "polybench-atax-c",
                 "polybench-bicg-c",
                 "polybench-cholesky-c",
                 "polybench-correlation-c",
                 "polybench-covariance-c",
                 "polybench-deriche-c",
                 "polybench-doitgen-c",
                 "polybench-durbin-c",
                 "polybench-fdtd-2d-c",
                 "polybench-floyd-warshall-c",
                 "polybench-gramschmidt-c",
                 "polybench-heat-3d-c",
                 "polybench-jacobi-1d-c",
                 "polybench-jacobi-2d-c",
                 "polybench-lu-c",
                 "polybench-ludcmp-c",
                 "polybench-mvt-c",
                 "polybench-nussinov-c",
                 "polybench-seidel-2d-c",
                 "polybench-trisolv-c"]


grand_table_order = [
    'chaos (p)', 'logging (p)', 'pyaes (p)',
    'spectral_norm (p)', 'deltablue (p)', 'go (p)', 'mdp (p)',
    'pyflate (p)', 'telco (p)', 'hexiom (p)',
    'nbody (p)', 'raytrace (p)', 'unpack_seq (p)', 'fannkuch (p)',
    'json_dumps (p)', 'pickle (p)', 'richards (p)', 'version (p)',
    'float (p)', 'json_loads (p)', 'pidigits (p)', 'scimark (p)',
    '2mm (c)', '3mm (c)', 'adi (c)', 'atax (c)', 'bicg (c)',
    'cholesky (c)', 'correlation (c)', 'covariance (c)', 'deriche (c)',
    'doitgen (c)', 'durbin (c)', 'fdtd-2d (c)', 'floyd-warshall (c)',
    'gramschmidt (c)',
    'heat-3d (c)', 'jacobi-1d (c)', 'jacobi-2d (c)', 'lu (c)',
    'ludcmp (c)', 'mvt (c)', 'nussinov (c)', 'seidel-2d (c)',
    'trisolv (c)',
    'get-time (p)', 'sentiment (p)', 'json (p)', 'md2html (p)',
    'base64 (p)', 'primes (p)',
    'get-time (n)', 'autocomplete (n)', 'json (n)', 'primes (n)',
    'img-resize (n)', 'base64 (n)', 'ocr-img (n)'
]

grand_table_pyperf = ['chaos (p)',
 'logging (p)',
 'pyaes (p)',
 'spectral (p)',
 'deltablue (p)',
 'go (p)',
 'mdp (p)',
 'pyflate (p)',
 'telco (p)',
 'hexiom (p)',
 'nbody (p)',
 'raytrace (p)',
 'unpack_seq (p)',
 'fannkuch (p)',
 'json_dumps (p)',
 'pickle (p)',
 'richards (p)',
 'version (p)',
 'float (p)',
 'json_loads (p)',
 'pidigits (p)',
 'scimark (p)']
grand_table_polybench = [
 '2mm (c)',
 '3mm (c)',
 'adi (c)',
 'atax (c)',
 'bicg (c)',
 'cholesky (c)',
 'correlation (c)',
 'covariance (c)',
 'deriche (c)',
 'doitgen (c)',
 'durbin (c)',
 'fdtd-2d (c)',
 'floyd-warshall (c)',
 'gramschmidt (c)',
 'heat-3d (c)',
 'jacobi-1d (c)',
 'jacobi-2d (c)',
 'lu (c)',
 'ludcmp (c)',
 'mvt (c)',
 'nussinov (c)',
 'seidel-2d (c)',
 'trisolv (c)']
grand_table_python =[
 'get-time (p)',
 'sentiment (p)',
 'json (p)',
 'md2html (p)',
 'base64 (p)',
 'primes (p)']
grand_table_node =[
 'get-time (n)',
 'autocomplete (n)',
 'json (n)',
 'primes (n)',
 'img-resize (n)',
 'base64 (n)',
 'ocr-img (n)']

decomposition_benchmarks = ['polybench-seidel-2d-c',
 'polybench-bicg-c',
 'get-time-python',
 'pyperf-fannkuch-python',
 'pyperf-telco-python',
 'markdown-to-html-python',
 'sentiment-analysis-python',
 'pyperf-mdp-python',
 'pyperf-pyflate-python',
 'autocomplete-node',
 'ocr-img-node',
 'polybench-heat-3d-c',
 'img-resize-node',
 'base64-node']
decomposition_benchmarks.reverse()

def smart_round(v):
    if v > 1000:
        return int(v)
    if v > 10:
        return (round(v,1))
    else:
        return(round(v,2))

def benchmarks_renamer(df):
    return df.replace(
    ["-node", "-python", "-c$", "polybench-", "pyperf-", "-analysis", "markdown-to-html", "baseline", "gh-nop", "groundhog-sd", "-t.-", "-t. ", "_sequence", "_norm"],
    [" (n)", " (p)", " (c)",     "",           "",       "",           "md2html","baseline","GH-NOP", "GH","-", " ", "_seq", ""],regex=True)

def create_dir(path):
    os.makedirs(path, exist_ok=True)

def plot_cdf(data, x_label='', y_label='', log=False, interval=None,
             color=None, ax=None, label=None,
             linewidth=None, style='-', alpha=1, y_lim=[0, 1], marker=None, markersize=None, title=''):
    if not ax:
        fig, ax = plt.subplots(nrows=1)
    ecdf = ECDF(data)
    ax.plot(ecdf.x, ecdf.y, '-', color=color, label=label,
            linewidth=linewidth, linestyle=style, alpha=alpha, marker=marker, markersize=markersize)
    ax.set_ylim(y_lim)
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    if interval:
        ax.xaxis.set_major_locator(ticker.MultipleLocator(interval))
    if log:
        ax.set_xscale('log')
    if label:
        plt.legend(loc='lower right')

    plt.title(title, fontsize=20)
    return ax


def cum_dist(data, x, y, logy=False, logx=False, style='-',
             density=False, filename=None, label=None, ax=None,
             linewidth=None, color=None, x_major_interval=None, x_minor_interval=None, title='',
             marker=None, markersize=None):
    value = data.sum() if density else 1
    ax = (data/value).cumsum().plot(ax=ax, drawstyle='steps',
                                    style=style, label=label, linewidth=linewidth, color=color, marker=None, markersize=None)
    ax.set_xlabel(x['label'])
    ax.set_ylabel(y['label'])
    plt.grid(which='major', axis='both', linestyle='--')
    plt.grid(which='minor', axis='both', linestyle=':')
    plt.xticks(rotation=0, ha="center")
    if x_minor_interval:
        ax.xaxis.set_minor_locator(ticker.MultipleLocator(x_minor_interval))
    if x_major_interval:
        ax.xaxis.set_major_locator(ticker.MultipleLocator(x_major_interval))

    if density:
        ax.set_ylim([0, 1])
        ax.yaxis.set_minor_locator(ticker.MultipleLocator(.1))
    if label:
        plt.legend()
    if logy:
        ax.set_yscale('log')
    if logx:
        ax.set_xscale('log')
    plt.title(title, fontsize=20)
    return ax


def plot_scatter(x, y, x_label='', y_label='', log_x=False, log_y=False,
                 color=None, ax=None, label=None, alpha=1, marker=None, size=None):

    ax = plt.scatter(x, y, alpha=alpha, label=label,
                     marker=marker, s=size, color=color)

    ax = plt.gca()

    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    plt.grid(which='major', axis='both', linestyle='--')
    plt.grid(which='minor', axis='both', linestyle=':')

    if log_x:
        ax.set_xscale('log')
    if log_y:
        ax.set_yscale('log')

    return ax

def bar_plot(pt_m, pt_e, bars, xlabel, ylabel, log=True, colors=['#008FD4', '#FC4F30'], title="", outname="",
             numbers=True, hatches=None, all_benchmarks=all_benchmarks,figsize=(80,15), legend_size=40, out_dir="/local/workspace/automation/plot"):
    if outname == "":
        outname = 'test'#''.join(random.choices(string.ascii_uppercase + string.digits, k=10))

    pt = pt_m.reindex(bars, axis=1)
    if isinstance(colors, list):
        ax = pt.plot.bar(figsize=figsize, width = 0.8, rot=90, color=colors[:len(bars)],
                         legend=False, yerr=pt_e.reindex(all_benchmarks))
    else:
        ax = pt.plot.bar(figsize=figsize, width = 0.8, rot=90, colormap=colors,
                         legend=False, yerr=pt_e.reindex(all_benchmarks))
    if log:
        plt.yscale('log')

    if hatches:
        hbars = ax.patches
        hbars.sort(key=lambda x: x.xy)

        for hbar, hatch in zip(hbars, hatches):
            hbar.set_hatch(hatch)

    if numbers:
        for p in ax.patches:

            if p.get_height() > 0:
                plt.text(p.get_x() * 1.0, p.get_height() * 1.1, f'{p.get_height():.0f}', fontsize=14 )


    plt.legend(prop={'size': legend_size}, loc='upper left', bbox_to_anchor=(0, 1.1), ncol=3)

    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    plt.title(title)
    plt.savefig(out_dir +  os.sep + outname + "-latency.png",bbox_inches="tight")
    plt.savefig(out_dir +  os.sep + outname + "-latency.pdf",bbox_inches="tight")


def bar_plot_duration(grouped_data, bars, xlabel, ylabel, log=True, colors=['#008FD4', '#FC4F30'], title="", outname="", numbers=True, hatches=None, all_benchmarks=all_benchmarks):
    m = grouped_data.mean().reset_index().query('name in @all_benchmarks')
    e = grouped_data.std().reset_index().query('name in @all_benchmarks')
    pt_m = m.pivot_table(index='name', columns='type', values='duration').reindex(all_benchmarks)
    pt_e = e.pivot_table(index='name', columns='type', values='duration').reindex(all_benchmarks)
    bar_plot(pt_m, pt_e, bars, xlabel, ylabel, log, colors, title, outname, numbers, hatches)

def bar_plot_reset(grouped_data, bars, xlabel, ylabel, log=True, colors=['#008FD4', '#FC4F30'], title="", outname="", numbers=False, hatches=None, all_benchmarks=all_benchmarks, figsize=(7.5,2), legend_size=14, out_dir="/local/workspace/automation/plot"):
    m = grouped_data.mean().reset_index().query('Benchmark in @all_benchmarks')
    e = grouped_data.std().reset_index().query('Benchmark in @all_benchmarks')
    pt_m = m.pivot_table(index='Benchmark', columns='type', values='mean_ms').reindex(all_benchmarks)
    pt_e = e.pivot_table(index='Benchmark', columns='type', values='mean_ms').reindex(all_benchmarks)
    bar_plot(pt_m, pt_e, bars, xlabel, ylabel, log, colors, title, outname, numbers, hatches, figsize=figsize, legend_size=legend_size, out_dir=out_dir)

def bar_plot_relative(pt_m, bars, xlabel, ylabel, log=True, colors=['#008FD4', '#FC4F30'], title="", outname="", numbers=True, hatches=None,
                      legend=True, legend_cols=4, bench_names=True, fig_size=(8,2.5), ylim=3.1,format_y=True, xput_ratios=[],print_actual_ratios=False, out_dir="/local/workspace/automation/plot"):
    if outname == "":
        outname = 'test'#''.join(random.choices(string.ascii_uppercase + string.digits, k=10))

    pt = pt_m
    if isinstance(colors, list):
        ax = pt.plot.bar(figsize=fig_size, width = 0.8, rot=90, color=colors[:len(bars)],
                         legend=False)
    else:
        ax = pt.plot.bar(figsize=fig_size, width = 0.8, rot=90, colormap=colors,
                         legend=False)
    if log:
        plt.yscale('log')

    if hatches:
        hbars = ax.patches
        hbars.sort(key=lambda x: x.xy)

        for hbar, hatch in zip(hbars, hatches):
            hbar.set_hatch(hatch)

    if numbers:
        for p in ax.patches:
            if p.get_height() > 0:
                plt.text(p.get_x() * 1.0, p.get_height() * 1.1, f'{p.get_height():.0f}', fontsize=14 )

    if len(xput_ratios):
        count = 0
        #print(len(bars))
        #print(len(ax.patches))
        #print(len(xput_ratios))
        #print(len(ax.patches)/len(bars), 2*len(ax.patches)/len(bars))
        for i,p in enumerate(ax.patches):
            #print(i,p)
            #if i % len(bars) == 0:
            if i < len(ax.patches)/len(bars):
                plt.text(p.get_x() * 1.0, ylim, f'{xput_ratios[count]:.2f}', fontsize=8)
                count+=1
            if print_actual_ratios:
                if len(ax.patches)/len(bars) < i <= 2*len(ax.patches)/len(bars):
                    #print(p.get_height())
                    plt.text(p.get_x()-0.2 * 1.0, ylim + .3, f'{p.get_height():.2f}', fontsize=8)


    plt.axhline(y = 1, color = 'b', linestyle = '--', lw=1)
    if legend:
        plt.legend(prop={'size': 14}, loc='upper left', bbox_to_anchor=(0, 1.5), ncol=legend_cols)
    plt.ylim(0,ylim)
    plt.ylabel(ylabel,labelpad=-1)
    if format_y:
        ax.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
    if not bench_names:
        x_axis = ax.axes.get_xaxis()
        x_axis.set_visible(False)
    else:
        plt.xlabel(xlabel)
    plt.title(title)
    plt.tight_layout()
    plt.savefig(out_dir +  os.sep + outname + ".png",bbox_inches="tight")
    plt.savefig(out_dir +  os.sep + outname + ".pdf",bbox_inches="tight")


def debug_xput(df):
    tmp_df = df.copy()
    tmp_df['start']=tmp_df['start'] - tmp_df['start'].iloc[0]
    tmp_df['start_time'] = pd.to_datetime(tmp_df['start'],unit='ms',origin='unix')
    tmp_df = tmp_df.set_index('start_time')
    reqs = tmp_df[['end_end_latency']].resample("1s").agg(['mean', 'count'])
    reqs[('end_end_latency', 'count')].rolling(2, center=True).mean().plot()

def mark_checkpoints(df):
    new_df = df.sort_values(by=['name', 'start']).reset_index()
    for i in range(1, len(new_df)):
        if new_df.loc[i-1, 'invocation_type'] == 'cold':
            if new_df.loc[i, 'invocation_type'] != 'cold':
                new_df.loc[i, 'invocation_type'] = 'checkpoint'
    return new_df

def calculate_xput(group):
    meaningful_data = group.sort_values(by='start')
    return (len(meaningful_data)) / ((meaningful_data.sort_values(by='end').iloc[-1]['end'] - meaningful_data.sort_values(by='start').iloc[0]['start'])/1000)

def calculate_duration(group):
    return (group.sort_values(by='end').iloc[-1]['end'] - group.sort_values(by='start').iloc[0]['start'])

def get_middle_group(group):
    data = group.sort_values(by='start')
    duration = calculate_duration(data)
    throw_away = duration/6
    start = data.iloc[0]['start'] + throw_away
    end = data.iloc[-1]['start'] - throw_away
    meaningful_data = data.query('start >= @start and start <= @end')
    return meaningful_data

def calculate_xput_middle(group):
    try:
        meaningful_data = get_middle_group(group)
        return (len(meaningful_data)-1) / ((meaningful_data.sort_values(by='end').iloc[-1]['end'] - meaningful_data.sort_values(by='start').iloc[0]['start'])/1000)
    except Exception:
        return 0

def calculate_xput_latency_middle(group):
    try:
        meaningful_data = get_middle_group(group)
        df = pd.Series([],dtype=float)
        df['xput'] = (len(meaningful_data)-1) / ((meaningful_data.sort_values(by='end').iloc[-1]['end'] - meaningful_data.sort_values(by='start').iloc[0]['start'])/1000)
        df['duration'] = group.duration.mean()
        df['latency'] = group.end_end_latency.mean()
        return df
    except Exception:
        return 0

def calculate_xput_cores_middle(group):
    try:
        meaningful_data = get_middle_group(group)
        df = pd.Series([],dtype=float)
        df['xput'] = (len(meaningful_data)-1) / ((meaningful_data.sort_values(by='end').iloc[-1]['end'] - meaningful_data.sort_values(by='start').iloc[0]['start'])/1000)
        df['CPUs'] = group.num_containers.mean()
        return df
    except Exception:
        return 0

def peak_calculate_xput(starts, ends):
    try:
        #print(ends.iloc[-1] - starts.iloc[0])
        duration = (ends.iloc[-1] - starts.iloc[0])/1000
        return len(starts)/duration
    except Exception:
        return 0

def calculate_xput_cores(group):
    try:
        meaningful_data = group.sort_values(by='start')
        count = len(meaningful_data.end)
        #print(count)
        increments=count//6
        xput_max = 0
        for i in range(0, 6):
            xput_max = max(xput_max,peak_calculate_xput(meaningful_data.start[i*increments:(i+1)*increments], meaningful_data.end[i*increments:(i+1)*increments]))
        #if the number of requests is too low, we consider the whole data without searching for the peak interval
        if count < 24:
            xput_max = max(xput_max,peak_calculate_xput(meaningful_data.start, meaningful_data.end))

        df = pd.Series([],dtype=float)
        df['xput'] = xput_max
        df['CPUs'] = group.num_containers.mean()
        return df
    except Exception:
        return 0

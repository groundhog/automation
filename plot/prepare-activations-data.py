#!/usr/bin/env python
# coding: utf-8
import sys

import pandas as pd
import os
import json as json

from utils import *
from glob import glob

root_dir = sys.argv[1]
config_file=root_dir + os.sep + "run_experiment_configs.txt"
configs = []
try:
    with open(config_file) as f:
        configs = f.readlines()[0].split(',')
except Exception:
        exit(0)
config_exp = configs[0].split('=')[1].split(' ')[1:]
config_run = [s.replace('-','') for s in configs[3].split('=')[1].split(' ')[1:]]
config_instrument = configs[2].split('=')[1]
try:
    config_gh_cores = configs[5].split('=')[1]
    config_num_containers = configs[6].split('=')[1]
except Exception:
    print("File not found: " + root_dir + os.sep + "run_experiment_configs.txt")
    pass
out_dir = root_dir + os.sep + "plots_" + '_'.join(config_exp)+"-"+'_'.join(config_run).replace('-','')+"-inst"+config_instrument
os.makedirs(out_dir, exist_ok=True)

baseline_dirs=sorted(glob(root_dir + os.sep + "*baseline"))
bl_refactored_dirs=sorted(glob(root_dir + os.sep + "*-refactored"))
forkline_dirs=sorted(glob(root_dir + os.sep + "*forkline"))
sd_dirs=sorted(glob(root_dir + os.sep + "*groundhog-sd"))
uffd_dirs=sorted(glob(root_dir + os.sep + "*groundhog-uffd"))
nop_dirs=sorted(glob(root_dir + os.sep + "*gh-nop"))
len(baseline_dirs), len(sd_dirs), len(nop_dirs)


exp_files = {}
forkline_files = []
for d in forkline_dirs:
    forkline_files.append(sorted(glob(d + os.sep + "[0-9]*-[0-9]*-*.csv")))
baseline_files = []
for d in baseline_dirs:
    baseline_files.append(sorted(glob(d + os.sep + "[0-9]*-[0-9]*-*.csv")))
bl_refactored_files = []
for d in bl_refactored_dirs:
    bl_refactored_files.append(sorted(glob(d + os.sep + "[0-9]*-[0-9]*-*.csv")))
sd_files = []
for d in sd_dirs:
    sd_files.append(sorted(glob(d + os.sep + "[0-9]*-[0-9]*-*.csv")))
uffd_files = []
for d in uffd_dirs:
    uffd_files.append(sorted(glob(d + os.sep + "[0-9]*-[0-9]*-*.csv")))
nop_files = []
for d in nop_dirs:
    nop_files.append(sorted(glob(d + os.sep + "[0-9]*-[0-9]*-*.csv")))

exp_files['baseline'] = baseline_files
exp_files['baseline-refactored'] = bl_refactored_files
exp_files['forkline'] = forkline_files
exp_files['groundhog-sd'] = sd_files
exp_files['groundhog-uffd'] = uffd_files
exp_files['gh-nop'] = nop_files

len(baseline_files), len(sd_files), len(nop_files)
print(len(baseline_files), len(sd_files), len(nop_files))

def mark_checkpoints(df):
    new_df = df.sort_values(by=['name', 'start']).reset_index()
    for i in range(1, len(new_df)):
        if new_df.loc[i-1, 'invocation_type'] == 'cold':
            if new_df.loc[i, 'invocation_type'] != 'cold':
                new_df.loc[i, 'invocation_type'] = 'checkpoint'
    return new_df

def prepare_dfs(benchmarks_files, run, mark_chk=False):
    benchmarks_dfs = []
    for i in range(len(benchmarks_files)):
        #try:
            print(len(benchmarks_files[i]))
            sub_dfs = []
            for f in benchmarks_files[i]:
                try:
                    sub_dfs.append(pd.read_csv(f, float_precision="legacy").sort_values(by=['name', 'start']))
                except Exception:
                    print("no data: ", f)
                    #pass

            print("processed:",len(sub_dfs))
            df = pd.concat(sub_dfs)
            df["iteration"] = i
            df["gh_cores"] = config_gh_cores
            df["num_containers"] = config_num_containers
            if mark_chk:
                benchmarks_dfs.append(mark_checkpoints(df))
            else:
                benchmarks_dfs.append(df)
            print(i)
        #except Exception:
        #    pass
    #try:
    final_df = pd.concat(benchmarks_dfs)
    final_df["run"] = run
    #final_df['end_end_latency'] = final_df['end_end_latency']*1e3
    final_df["type"] = final_df["invocation_type"] + "-" + final_df["run"]
    #except Exception:
    #    pass
    return final_df

print("------------------------------------")

print(config_exp)
print("------------------------------------")
benchmarks = {}
dfs = []
for e in config_exp:
    #try:
        if ("scale_out" not in config_run)and ("groundhog" in e or "gh-nop" in e):
            print(len(exp_files[e]))
            benchmarks[e] = prepare_dfs(exp_files[e], e, True)
        else:
            print(len(exp_files[e]))
            #print(e)
            benchmarks[e] = prepare_dfs(exp_files[e], e)
        dfs.append(benchmarks[e])
    #except Exception:
    #    pass
df = pd.concat(dfs)


df_success = df.query('statusCode == True or statusCode == 0')
df_failures = df.query('statusCode != True and statusCode != 0')

print("Failed:")
print(df_failures['name'].value_counts())
print("#Failed Records: "+ str(df_failures['name'].value_counts()), file=sys.stderr)

print("Successful:")
print(df_success['name'].value_counts())
print("#Records: "+ str(df_success['name'].value_counts()), file=sys.stderr)

print("Value Counts")
print(df_success.groupby(['name','type'])['invocation_type'].value_counts().to_string())
print(df_success.groupby(['name','type'])['invocation_type'].value_counts().to_string(), file=sys.stderr)

df.to_csv(out_dir+ os.sep + "activations-data.csv")
print("Data written to: "+ out_dir+ os.sep + "activations-data.csv", file=sys.stderr)


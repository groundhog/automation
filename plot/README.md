# Plotting data
Install the prerequisites:
```
$ pip install -r requirements.txt
```
First prepare the data by running:
```
$ ./prepare_data.sh <dir>
```
where `<dir>` is the raw data directory outputted in (/local/workspace/automation/benchmarks/benchmarks/) by the experiment script in (/local/workspace/automation/experiments/\*.sh)


Launch jupyter-lab by executing:
```
$ jupyter-lab
```

If you are running jupyter-lab on the controller node, you can access locally after forwarding its port:
```
$ ssh -L8888:localhost:8888 $UNAME@$SERVER
```

Open PLOTTER-EUROSYS23.ipynb and update the `Data source` top section to point to the new directories and run the notebook.

Full-length paper data can be found at (https://groundhog.mpi-sws.org/downloads/DATA-EUROSYS23.tgz), if extracted at /local/workspace/automation/benchmarks/benchmarks/EUROSYS23-DATA, the plotter will plot all the paper results.

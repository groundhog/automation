plot_group=$1
exptime=$(date +"%Y_%m_%d-%H_%M_%S")
tar -czvf ${plot_group}/`basename ${plot_group}`-rawdata-bk-$exptime.tgz $plot_group
for d in $plot_group/[0-9]*; do mkdir -p $d/combined-data && mv $d/[0-9]*/* $d/combined-data/; done
for d in $plot_group/[0-9]*/combined-data; do echo $d && time python3 prepare-activations-data.py $d; done >> $plot_group/data_prep.out

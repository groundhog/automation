# This script should be run on a freshly created Ubuntu VM
sudo apt-get update

# /local/workspace is our playground
sudo mkdir -p /local/workspace
chown ubuntu:ubuntu /local/workspace
sudo chown ubuntu:ubuntu /local/workspace

# let's clone all repos
cd  /local/workspace
git clone https://gitlab.mpi-sws.org/groundhog/openwhisk.git
git clone https://gitlab.mpi-sws.org/groundhog/openwhisk-cli.git /local/workspace/openwhisk/openwhisk-cli
git clone --recurse-submodules https://gitlab.mpi-sws.org/groundhog/openwhisk-runtime-nodejs.git
git clone --recurse-submodules https://gitlab.mpi-sws.org/groundhog/openwhisk-runtime-python.git
git clone --recurse-submodules https://gitlab.mpi-sws.org/groundhog/openwhisk-runtime-c.git
git clone https://gitlab.mpi-sws.org/groundhog/automation.git
git clone https://gitlab.mpi-sws.org/groundhog/groundhog.git

# let's install OpenWhisk
cd /local/workspace/openwhisk/tools/ubuntu-setup
./all.sh
cd /local/workspace/openwhisk/openwhisk-cli
./gradlew releaseBinaries -PnativeBuild
cp /local/workspace/openwhisk/openwhisk-cli/build/wsk /local/workspace/openwhisk/bin/
sudo cp /local/workspace/openwhisk/bin/wsk* /usr/bin/
sudo cp /local/workspace/openwhisk/bin/wsk* /usr/local/bin/
wsk property set --apihost 127.0.0.1
wsk property set --auth `cat /local/workspace/openwhisk/ansible/files/auth.guest`
sudo chown $USER:$USER  /var/run/docker.sock
cd /local/workspace/openwhisk
./gradlew distDocker

sudo apt-get install -y python-is-python3 python3-pip
sudo ln -s /usr/bin/pip3 /usr/bin/pip
sudo mv /usr/local/bin/pip /usr/local/bin/pip-old
sudo ln -s /usr/local/bin/pip3 /usr/local/bin/pip

# Now let's build pull all runtimes layers to have them in the VM-image (reducing build time afterwards)
cd /local/workspace/openwhisk-runtime-python
./gradlew core:python3ActionLoop:distDocker
cd /local/workspace/openwhisk-runtime-c
./gradlew core:cActionLoop:distDocker
cd /local/workspace/openwhisk-runtime-nodejs
./gradlew core:nodejs14Action:distDocker

# Let's build the images of the benchmarks (some of them have their own images)
cd /local/workspace/automation/benchmarks
sudo apt-get install -y python-is-python3
pip install -r requirements.txt
./deploy_benchmarks.sh 1

cd /local/workspace/automation/prepare-vm
cp id_groundhog* ~/.ssh
cat id_groundhog.pub >> ~/.ssh/authorized_keys
sudo chmod -R 600 ~/.ssh
sudo chmod 700 ~/.ssh

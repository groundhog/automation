source configs.env
source cmd.sh

user="ubuntu"
benchmarks_dir="/local/workspace/automation/benchmarks"
#  do_cmd
#  do_bg=0 ## default value
#  ruser=$1
#  host=$2
#  cmd=$3
#  do_bg=$4
#  out_null=$5

echo "Deploying with options: $@"

(do_cmd $user $CORE_MACHINE "cd $benchmarks_dir; git pull; bash -x tag.sh $@ && bash -x tag.sh $@ && ./deploy_benchmarks.sh && ./deploy_benchmarks.sh")&
(do_cmd $user $INVOKER_MACHINE "cd $benchmarks_dir; git pull;  bash -x tag.sh $@ && bash -x tag.sh $@ && ./deploy_benchmarks.sh 1 ")&
(do_cmd $user $CLIENT_MACHINE "cd $benchmarks_dir; git pull;")&

wait $(jobs -p)

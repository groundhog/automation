source configs.env
source cmd.sh
num_cpus=${1:-1}
num_vm_cores=${2:-0}
user="ubuntu"
ow_dir="/local/workspace/openwhisk"

hostname=`hostname`
hostname_index=`hostname | cut -d"-" -f3`
CORE_MACHINE="$hostname"
INVOKER_MACHINE="ow-invoker0-$hostname_index"
CLIENT_MACHINE="ow-client-$hostname_index"

num_available_cpus=$(do_cmd "ubuntu" $INVOKER_MACHINE "nproc --all")

(do_cmd $user $CORE_MACHINE "sudo systemd-resolve --flush-caches")
(do_cmd $user $INVOKER_MACHINE "sudo systemd-resolve --flush-caches")
(do_cmd $user $CLIENT_MACHINE "sudo systemd-resolve --flush-caches")

echo $CORE_MACHINE $INVOKER_MACHINE $CLIENT_MACHINE
./set_environment.sh $CORE_MACHINE $INVOKER_MACHINE $CLIENT_MACHINE

sudo cp /home/ubuntu/.ssh/* /root/.ssh/
do_cmd "ubuntu" $INVOKER_MACHINE "sudo cp /home/ubuntu/.ssh/* /root/.ssh/"
do_cmd "ubuntu" $CLIENT_MACHINE "sudo cp /home/ubuntu/.ssh/* /root/.ssh/"

do_cmd "root" $INVOKER_MACHINE "echo on > /sys/devices/system/cpu/smt/control"
for i in `seq 1 $(($num_available_cpus - 1))`;
do
	do_cmd "root" $INVOKER_MACHINE "echo 1 > /sys/devices/system/cpu/cpu$i/online"
done
do_cmd "root" $INVOKER_MACHINE "echo off > /sys/devices/system/cpu/smt/control"
do_cmd "root" $INVOKER_MACHINE "echo 2 > /proc/sys/kernel/randomize_va_space"

do_cmd "root" $INVOKER_MACHINE "echo '0-$(($num_available_cpus - 1))' > /sys/fs/cgroup/cpuset/docker/cpuset.cpus"

if [ $num_vm_cores -ne 0 ];
then
	for i in `seq $num_vm_cores $(($num_available_cpus - 1))`;
	do
		do_cmd "root" $INVOKER_MACHINE "echo 0 > /sys/devices/system/cpu/cpu$i/online"
	done

	do_cmd "root" $INVOKER_MACHINE "echo '0-$(($num_vm_cores - 1))' > /sys/fs/cgroup/cpuset/docker/cpuset.cpus"
fi


#Checkput and pull openwhisk
(do_cmd $user $CORE_MACHINE "cd $ow_dir; git checkout .; git pull")
(do_cmd $user $INVOKER_MACHINE "cd $ow_dir; git checkout .; git pull")

#Configure number of CPUs
(do_cmd $user $CORE_MACHINE "sed -i 's/\"--cpus\" -> Set(\"..0\")/\"--cpus\" -> Set(\"$num_cpus.0\")/g' /local/workspace/openwhisk/core/invoker/src/main/scala/org/apache/openwhisk/core/invoker/InvokerReactive.scala")
(do_cmd $user $INVOKER_MACHINE "sed -i 's/\"--cpus\" -> Set(\"..0\")/\"--cpus\" -> Set(\"$num_cpus.0\")/g' /local/workspace/openwhisk/core/invoker/src/main/scala/org/apache/openwhisk/core/invoker/InvokerReactive.scala")


#Build
(do_cmd $user $CORE_MACHINE "cd $ow_dir; ./gradlew distDocker")&
(do_cmd $user $INVOKER_MACHINE "cd $ow_dir; ./gradlew distDocker")&

wait $(jobs -p)

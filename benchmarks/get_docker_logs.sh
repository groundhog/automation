#!/bin/bash
source configs.env
source cmd.sh
outdir=$1
benchmark=$2
suffix=$3

cmd="docker ps | grep -e \"whisk/action.*latest\" | cut -d\" \" -f1"
#c_id = container_id.stdout.decode("utf-8")[:-1]
#subprocess.run(f"docker logs {c_id} > {outdir}/{benchmark}{suffix}.json", shell=True)
c_id=$(do_cmd root $INVOKER_MACHINE "$cmd")
#cmd2="docker logs $c_id > $outdir/${benchmark}${suffix}.json"
cmd2="docker logs $c_id"

sleep 10
do_cmd root $INVOKER_MACHINE "$cmd2" > $outdir/${benchmark}-docker-logs${suffix}.json 2>&1

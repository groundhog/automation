plot_group=$1
#for d in $plot_group/*/*/plot*; do mkdir -p $plot_group/plots/`basename $d` && cp $d/*.pdf  $plot_group/plots/`basename $d`/ && cp $d/*_table  $plot_group/plots/`basename $d`/; done
for d in $plot_group/*/*/plot*; do  new_name=`basename $d`-$(echo $(basename $(dirname $(dirname $d))) | cut -d'-' -f 3) && mkdir -p $plot_group/plots/$new_name && cp $d/*.pdf  $plot_group/plots/$new_name/ && cp $d/*_table  $plot_group/plots/$new_name/; done
#for d in $plot_group/*/*/plot*; do
#	echo "`basename $d`-$(basename $(dirname $(dirname $d)))";
#done
zip -r $plot_group/plots.zip  $plot_group/plots/

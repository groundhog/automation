source cmd.sh
user="ubuntu"
skip_deployment=${1:-1}
wipe_deployment=${2:-0}

cd /local/workspace/automation/benchmarks
sudo chown -R $USER:$USER /local/workspace
git pull

hostname=`hostname`
hostname_index=`hostname | cut -d"-" -f3`
CORE_MACHINE="$hostname"
INVOKER_MACHINE="ow-invoker0-$hostname_index"
CLIENT_MACHINE="ow-client-$hostname_index"

echo $CORE_MACHINE $INVOKER_MACHINE $CLIENT_MACHINE

number_cpus=$(do_cmd "ubuntu" $INVOKER_MACHINE "nproc")
num_containers=${3:-$(($number_cpus))}

#sudo docker system prune -a -f
#sudo docker rm -v $(sudo docker ps -a -q -f status=exited)
#sudo docker rmi -f  $(sudo docker images -f "dangling=true" -q)
#docker volume ls -qf dangling=true | xargs -r docker volume rm

#do_cmd $user $INVOKER_MACHINE "sudo docker system prune -a -f"
#do_cmd $user $INVOKER_MACHINE "sudo docker rmi -f  $(sudo docker images -f \"dangling=true\" -q)"
#do_cmd $user $INVOKER_MACHINE "docker volume ls -qf dangling=true | xargs -r docker volume rm"


echo "maximum number of containers allowed = 1 or 2x#CPUs: $num_containers"
./set_environment.sh $CORE_MACHINE $INVOKER_MACHINE $CLIENT_MACHINE

memory_per_container=2048
total_memory=$(($num_containers*memory_per_container))

./keep_pinging_till_up.sh $INVOKER_MACHINE


cd /local/workspace/openwhisk/ansible
git checkout .
git pull
find . -type f -name "*" -exec sed -i "s/ow-core/$CORE_MACHINE/g" {} +
find . -type f -name "*" -exec sed -i "s/ow-invoker0/$INVOKER_MACHINE/g" {} +

sudo chown $USER:$USER  /var/run/docker.sock

if [ $wipe_deployment = 1 ]; then
	ansible-playbook -i environments/distributed/ wipe.yml
fi

if [ $skip_deployment = 1 ]; then
	cd -
	exit 0
fi



ansible-playbook -i environments/distributed/ prereq.yml
ansible-playbook -i environments/distributed/ setup.yml
ansible-playbook -i environments/distributed/ couchdb.yml
ansible-playbook -i environments/distributed/ initdb.yml
ansible-playbook -i environments/distributed/ wipe.yml
#ansible-playbook -i environments/distributed/ -e limit_action_memory_max=4294967296 -e limit_action_memory_std=2147483648 -e limit_invocations_per_minute=999999 -e limit_invocations_concurrent=999999 -e skip_pull_runtimes=True -e  invoker_user_memory="${total_memory}m" openwhisk.yml
ansible-playbook -i environments/distributed/ -e limit_action_memory_max=4294967296 -e limit_action_memory_std=2147483648 -e limit_invocations_per_minute=999999 -e limit_invocations_concurrent=999999 -e skip_pull_runtimes=True -e  invoker_user_memory="${total_memory}m" -e invoker_loglevel="ERROR" -e controller_loglevel="ERROR" openwhisk.yml
ansible-playbook -i environments/distributed/ postdeploy.yml
ansible-playbook -i environments/distributed/ apigateway.yml
ansible-playbook -i environments/distributed/ routemgmt.yml

wsk property set --apihost 127.0.0.1
wsk property set --auth `cat /local/workspace/openwhisk/ansible/files/auth.guest`

cd -

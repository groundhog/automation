#!/bin/bash
# File              : do_mpstats.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 03.12.2020
# Last Modified Date: 05.12.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
user="groundhog"
ruser="root"
host="localhost"
source cmd.sh
file=$1
end=$2

if [ $end = 0 ]; then
	start_mpstat $ruser $host $file
fi

if [ $end = 1 ]; then
	stop_mpstat $ruser $host
fi

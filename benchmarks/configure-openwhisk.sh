source cmd.sh
source configs.env
number_cpus=$(do_cmd "ubuntu" $INVOKER_MACHINE "nproc")
num_containers=${1:-$(($number_cpus*1))}
memory_per_container=2048
total_memory=$(($num_containers*memory_per_container))

echo "maximum number of containers allowed = 1 or 1x#CPUs: $num_containers"

#sed -i "s/userMemory.*/userMemory: \"{{ invoker_user_memory | default('${total_memory}m') }}\"/g" /local/workspace/openwhisk/ansible/group_vars/all

cd /local/workspace/openwhisk/ansible
sudo ansible-playbook -i environments/distributed/ -e limit_action_memory_max=4294967296 -e limit_action_memory_std=2147483648 -e limit_invocations_per_minute=999999 -e limit_invocations_concurrent=999999 -e skip_pull_runtimes=True -e  invoker_user_memory="${total_memory}m" openwhisk.yml
cd -

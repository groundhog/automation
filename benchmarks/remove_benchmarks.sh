#!/bin/bash
# File              : remove_benchmarks.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 25.10.2020
# Last Modified Date: 25.10.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
benchmarks=('get-time-c' 'get-time-python' 'get-time-node' 'base64-python' 'base64-node' 'json-python' 'json-node' 'primes-python' 'primes-node' 'autocomplete-node' 'markdown-to-html-python' 'img-resize-node' 'ocr-img-node' 'sentiment-analysis-python')

for b in "${benchmarks[@]}"
do
	echo Removing $b
	wsk -i action delete $b
done

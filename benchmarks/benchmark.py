#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# File              : benchmark.py
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 25.10.2020
# Last Modified Date: 06.12.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>

#curl -k -u $WSK_AUTH -L https://139.19.171.74:31001/api/v1/namespaces/guest/activations?limit=1

import numpy as np
import pandas as pd
import requests
import argparse
import json
import subprocess
import time
from time import sleep
import os
from pandas import json_normalize
from flatten_json import flatten
import multiprocessing
from datetime import datetime
import socket

SRC_DIR="/local/workspace/automation/benchmarks"
requests.packages.urllib3.disable_warnings()

OW_APIHOST = socket.gethostname()
#OW_APIPORT = 31001
WSK_AUTH = '23bc46b1-71f6-4ed5-8c54-816aa4f8c502:123zO3xZCLrMN6v2BKK1dXYFpXlPkccOFqm12CdAsMgRU4VrNZ9lyGVCGuMDGIwP'
WSK_AUTH_USER, WSK_AUTH_PASS = WSK_AUTH.split(':')
#URL_ACTIONS = f'https://{OW_APIHOST}:{OW_APIPORT}/api/v1/namespaces/_/actions'
URL_ACTIONS = f'https://{OW_APIHOST}/api/v1/namespaces/guest/actions'
#URL_ACTIVATIONS = f'https://{OW_APIHOST}:{OW_APIPORT}/api/v1/namespaces/_/activations'
URL_ACTIVATIONS = f'https://{OW_APIHOST}/api/v1/namespaces/guest/activations'

default_cold_count = 10
default_warm_count = 200
default_pause_time = 0
default_thpt_count = 1000
default_inflight_count = 5000
openloop = False


low_latency_benchmarks=["get-time-python",
            "get-time-node",
            "json-python",
            "json-node",
            "autocomplete-node",
            "markdown-to-html-python",
            "sentiment-analysis-python",
            "pyperf-deltablue-python",
            "pyperf-unpack_sequence-python",
            "pyperf-fannkuch-python",
            "pyperf-version-python",
            "pyperf-float-python",
            "polybench-atax-c",
            "polybench-bicg-c",
            "polybench-durbin-c",
            "polybench-gesummv",
            "polybench-jacobi-1d",
            "polybench-trisolv",
            "memtest-dirtying"]

def timed_execution(function):
    def wrapper(arg):
        t1 = time.time()
        function(arg)
        t2 = time.time()
        return (t2 - t1)
    return wrapper

def do_request(url, params=None):
    resp = requests.get(url=url, params=params,
            auth=(WSK_AUTH_USER, WSK_AUTH_PASS), verify=False)
    data = None
    try:
        data = resp.json() # Check the JSON Response Content documentation below
    except Exception:
        print(resp.text)
    return data

def get_benchmark_dir(benchmark):
    b = benchmark.split('-')
    subdir = b[-1]
    maindir = "func/" + '-'.join(b[:-1])
    return maindir

def do_deploy(benchmark):
    maindir = get_benchmark_dir(benchmark)
    print("Deploying: " + str(maindir))
    cmd = f'cd {maindir} && pwd && ./deploy.sh && cd ../'.split(' ')
    r = subprocess.run(cmd)
    print("Successfully deployed" if not r else "Failed")

@timed_execution
def run_benchmark(cmd):
    return subprocess.run(cmd)


def dump_docker_logs(benchmark, benchmark_dir, suffix=""):
    global outdir
    cmd = f"./eval.sh {benchmark} {benchmark_dir}/sample-params-secfaas.json".split(' ')
    run_benchmark(cmd)

    #container_id = subprocess.run('docker ps | grep -e "whisk/action.*latest" | cut -d" " -f1', shell = True,stdout=subprocess.PIPE)
    #print(container_id.stdout, outdir)
    #c_id = container_id.stdout.decode("utf-8")[:-1]
    #subprocess.run(f"docker logs {c_id} > {outdir}/{benchmark}{suffix}.json", shell=True)
    subprocess.run(f"./get_docker_logs.sh {outdir} {benchmark} {suffix}", shell=True)
    act = get_activations()
    try:
        a_id = act[0]['activationId']
        logs_activation = get_activation_logs(a_id)
        #if len(logs_activation) < 20:
        #    dump_docker_logs(benchmark, benchmark_dir)
        #else:
        with open(f"{outdir}/{benchmark}-logs{suffix}.json", 'w') as f:
            json.dump(logs_activation, f)
    except Exception:
        print(act)

    #print(subprocess.run(f"docker logs {c_id}", stdout=subprocess.PIPE, shell = True).stdout)

def do_benchmark(benchmark, is_cold=None, count=1):
    end_end_lat = []
    benchmark_dir = get_benchmark_dir(benchmark)
    for i in range(count):
        if is_cold:
            subprocess.run(["./kill_containers-distributed.sh"])
            #sleep(1)
        print(f"Iteration {i}")
        cmd = f"./eval.sh {benchmark} {benchmark_dir}/sample-params.json".split(' ')
        #tt = timeit(subprocess.run(cmd, number=1)
        tt = run_benchmark(cmd)
        end_end_lat.append(tt)
        #if "node" in benchmark:
        #sleep(1)
        #print(tt)
    if not is_cold and count > 1:
        dump_docker_logs(benchmark, benchmark_dir)
    return end_end_lat

def do_benchmark_activations(benchmark, is_cold=None, count=1, pause=0):
    all_activation_ids = []
    benchmark_dir = get_benchmark_dir(benchmark)
    for i in range(count):
        if is_cold:
            subprocess.run(["./kill_containers-distributed.sh"])
            #sleep(1)
        print(f"Iteration {i}")
        cmd = f"./eval.sh {benchmark} {benchmark_dir}/sample-params.json".split(' ')
        #cmd = f"./eval-async.sh {benchmark} {benchmark_dir}/sample-params.json {count} 0 1".split(' ')
        #tt = timeit(subprocess.run(cmd, number=1)
        #tt = run_benchmark(cmd)
        start_ms = time.time_ns()
        output = subprocess.run(cmd, capture_output=True)
        end_end_ms = (time.time_ns() - start_ms ) // 1e6
        #all_activation_ids.append(output.stdout.decode().split(' ')[-1][:32])
        try:
            activation = json.loads(output.stdout.decode())
            if ("TrueTime" in activation["response"]["result"]):
                activation['TrueTime'] =  activation["response"]["result"]['TrueTime']
            del activation["response"]["result"]
            activation['end_end_latency'] = end_end_ms
            all_activation_ids.append(activation)
        except Exception:
            print(output.stdout.decode())
        #if "node" in benchmark:
        if pause:
            sleep(pause/1000.0)
    if not is_cold and count > 1:
        dump_docker_logs(benchmark, benchmark_dir)
    end_end_lat = ["-1"] * len(all_activation_ids)
    print("Benchmark Done!", len(all_activation_ids))
    #print(all_activation_ids)
    return (end_end_lat, all_activation_ids)

# Returns timings + list of activations
def do_thpt_benchmark(benchmark, count=1, collect_stats=0, duration= 10, inflight_count=default_inflight_count, local_client=0, numWorkers=2, irq_delay=0):
    print("Starting the xput benchmark")
    print(time.time())

    benchmark_dir = get_benchmark_dir(benchmark)
    #We want it warm
    #cmd = f"./eval.sh {benchmark} {benchmark_dir}/sample-params.json {count}".split(' ')
    #run_benchmark(cmd)
    cmd = f"./eval-async.sh {benchmark} {benchmark_dir}/sample-params.json {count} {duration} {inflight_count} {local_client} {numWorkers} {irq_delay}".split(' ')
    last_activation = ""
    all_activation_ids = []
    all_activations = []
    if openloop:
        while True:
            try:
                output = subprocess.run(cmd, capture_output=True)
                #activations_list = output.stdout.decode().split('\n')
                all_activation_ids = output.stdout.decode().split(',')
                print(len(all_activation_ids))
                if len(all_activation_ids) == 1:
                    print(all_activation_ids)
                #for l in activations_list:
                #    li = l.decode()
                #    if li != "":
                #        all_activation_ids.append(li.split(' ')[-1])

                #last_activation = output.stdout.decode().split('\n')[-2].split(' ')[-1]
                last_activation = all_activation_ids[-1][:32]
                print(last_activation)
                if len(last_activation) == 1:
                    raise Exception
                break
            except Exception:
                print(output.stdout.decode())
                restart_cmd = "./restart-distributed.sh"
                subprocess.run(restart_cmd, shell=True)

        while True:
            last_act_json = get_activations(activationID=last_activation);
            print(last_act_json)
            #act_ids = []
            #for a in acts:
            #    act_ids.append(a['activationId'])
            #sleep(10);
            #if  last_activation in act_ids:
            #    break
            sleep(30)
            if "annotations" in last_act_json:
                break

        if collect_stats:
            if len(all_activation_ids) > 200000:
                pool = multiprocessing.Pool(processes = 32)
                all_activation_ids_np = np.array(all_activation_ids)
                all_activations = pool.map(get_array_activations, np.array_split(all_activation_ids_np, 32))
            else:
                for actID in all_activation_ids:
                    all_activations.append(get_activations(activationID=actID[:32]))
    else:
        #closed loop
        print("Sending out the requests")
        print(time.time())
        while True:
            try:
                output = subprocess.run(cmd, capture_output=True)
                #print(output.stdout.decode())
                print("Requests done, starting decoding")
                print(time.time())

                all_activations = json.loads(output.stdout.decode())
                print(len(all_activations))
                if len(all_activations) <= 1:
                        raise Exception
                break
            except Exception:
                print(output.stdout.decode())

        if local_client:
            dump_docker_logs(benchmark, benchmark_dir)


    #print(all_activation_ids)
    #print(len(all_activation_ids))
    #if count > 1:
    #    dump_docker_logs(benchmark, benchmark_dir, "-thpt")

    end_end_lat = ["-2"]*len(all_activations)
    return (end_end_lat, all_activations)

def search_dicts(dicts, key='key', value='value'):
    return list(filter(lambda d: d[key] == value, dicts))

def prepare_dataframe(activations):
    print("Preparing dataframe")
    print(time.time())
    coi = ['namespace', 'name', 'version', 'activationId',
            'start', 'end', 'duration', 'TrueTime', 'statusCode',
            'waitTime', 'kind', 'timeout', 'initTime']#, 'end_end_latency']
    #df = pd.DataFrame(columns=coi)
    dict_data = {}
    i = 0
    for act in activations:
        for activation in act:
            try:
                annotations = activation['annotations']
            except Exception:
                print(activation)
                continue

            waitTime = search_dicts(annotations, value='waitTime')
            kind = search_dicts(annotations, value='kind')
            timeout =search_dicts(annotations, value='timeout')
            initTime = search_dicts(annotations, value='initTime')
            try:
                if "TrueTime" not in activation and "response" in activation:
                    activation['TrueTime'] =  activation["response"]["result"]['TrueTime']
            except Exception:
                activation['TrueTime'] = -1

            dict_data[i] = {
                'namespace': activation['namespace'],
                'name': activation['name'],
                'version': activation['version'],
                'activationId': activation['activationId'],
                'start': activation['start'],
                'end': activation['end'],
                'duration': activation['duration'],
                'TrueTime': activation['TrueTime'],
                'end_end_latency': activation['end_end_latency'],
                'statusCode': activation['statusCode'] if 'statusCode' in activation \
                    else activation['response']['success'],
                #'end_end_latency': activation['end_end_latency'],
                'waitTime': waitTime[0]['value'] if len(waitTime) else -1,
                'kind': kind[0]['value'],
                'timeout': timeout[0]['value'],
                'initTime': initTime[0]['value'] if len(initTime) else -1,
                'invocation_type': 'cold' if len(initTime) else 'warm' }

            i = i + 1
    df = pd.DataFrame.from_dict(dict_data, 'index')
    print("Dataframe Ready")
    print(time.time())
    return df


def get_array_activations(activationIDs = None):
    list_acts = []
    for a in activationIDs:
        do_request(URL_ACTIVATIONS + "/" + a)
        list_acts.append(do_request(URL_ACTIVATIONS + "/" + a))
    return list_acts


def get_activations(limit=1, activationID = None):
    if activationID is None:
        do_request(URL_ACTIVATIONS, {"limit":limit})
        return do_request(URL_ACTIVATIONS, {"limit":limit})
    else:
        do_request(URL_ACTIVATIONS + "/" + activationID)
        return do_request(URL_ACTIVATIONS + "/" + activationID)


def get_activation_logs(activation_id):
    do_request(URL_ACTIVATIONS+"/"+activation_id+"/logs")
    return do_request(URL_ACTIVATIONS+"/"+activation_id+"/logs")



def persist_stats(end_end_lat, limit=1, outdir="benchmarks", activations=None, suffix=""):
    #due to a bug, we need to get the activations twice
    #activations = do_request(URL_ACTIVATIONS, {"limit":count})
    #activations = do_request(URL_ACTIVATIONS, {"limit":count})
    #upto = time.time()
    print("writing logs")
    print(time.time())
    if not activations:
        activations = [get_activations(limit)]
    #for act in reversed(activations):
    #    for i, activation in enumerate(act): #activations are retrieved in reverse order (last activation first)
    #        activation["end_end_latency"] = end_end_lat.pop()

    #dict_flattened = (flatten(activation, '.') for activation in activations)

    #df = pd.DataFrame.from_dict(json_normalize(clean_dat(activations)))
    df = prepare_dataframe(activations)
    now = datetime.now()
    date_time = now.strftime("%Y%m%d-%H%M%S")
    df.to_csv(outdir + os.sep + date_time+suffix+".csv", sep=',', encoding='utf-8')
    print("Done writing logs")
    print(time.time())

def main():
    global outdir
    parser = argparse.ArgumentParser(description='Openwhisk Benchmark')
    #parser.add_argument('--deploy', dest='do_deploy', action='store_true')
    parser.add_argument('--out', dest='outdir', action='store', default="benchmarks")
    parser.add_argument('--cold', dest='is_cold', action='store_true')
    parser.add_argument('--cold_count', dest='cold_count', type= int, action='store', default=default_cold_count)
    parser.add_argument('--warm', dest='is_warm', action='store_true')
    parser.add_argument('--warm_count', dest='warm_count', type= int, action='store', default=default_warm_count)
    parser.add_argument('--pause', dest='pause_time', type= int, action='store', default=default_pause_time)
    parser.add_argument('--thpt', dest='do_thpt', action='store_true')
    parser.add_argument('--thpt_count', dest='thpt_count', type= int, action='store', default=default_thpt_count)
    parser.add_argument('--req_container', dest='req_container', type= int, action='store', default=2)
    parser.add_argument('--thpt_duration', dest='thpt_duration', type= int, action='store', default=120)
    parser.add_argument('--scale_out', dest='scale_out', action='store_true')
    parser.add_argument('--micro_dirty', dest='micro_dirty', nargs='*', action='store', default=None)
    parser.add_argument('--micro_nmaps', dest='micro_nmaps', nargs='*', action='store', default=None)
    parser.add_argument('--micro_npages', dest='micro_npages', nargs='*', action='store', default=None)
    parser.add_argument('--scale_factor', dest='scale_factor', type= int, action='store', default=4)
    parser.add_argument('--benchmark', dest='benchmarks',
                                nargs='*', action='store', default=None)
    parser.add_argument('--analyze', dest='analyze', type=int,
                                            action='store', default=None)
    parser.set_defaults(is_cold=None)
    parser.set_defaults(is_warm=None)
    parser.set_defaults(do_thpt=None)
    parser.set_defaults(scale_out=None)
    parser.set_defaults(micro_dirty=None)

    args = parser.parse_args()
    #check if benchmark(s) are deployed
    cold_count = args.cold_count
    warm_count = args.warm_count
    pause_time = args.pause_time
    thpt_count = args.thpt_count
    thpt_duration = args.thpt_duration
    scale_factor = args.scale_factor
    req_container = args.req_container
    outdir = args.outdir
    if args.analyze:
        persist_stats([0 for i in range(args.analyze)], limit=args.analyze, outdir=outdir)
        exit(0)
    print(do_request(URL_ACTIONS))
    inflight_count=1
    if args.micro_dirty != None:
        if  args.micro_nmaps == None:
             args.micro_nmaps = [100] * len(args.micro_dirty)
        if  args.micro_npages == None:
             args.micro_npages = [1000] * len(args.micro_dirty)
        print ( args.micro_dirty,  args.micro_nmaps,  args.micro_npages)

        for i in range(len(args.micro_dirty)):
            end_to_end_timnings = []
            activations = []
            thpt_ctivations = []

            subprocess.run(["./kill_containers-distributed.sh"])
            subprocess.run("cd /local/workspace/automation/benchmarks/func/memtest-dirtying && ./change_dirtying_percent.sh "+str(args.micro_dirty[i]) + " " + str(args.micro_nmaps[i]) + " " + str(args.micro_npages[i])  , shell=True)
            benchmark_dir = get_benchmark_dir("memtest-dirtying-c")
            cmd = f"./eval.sh memtest-dirtying-c {benchmark_dir}/sample-params.json".split(' ')
            print("Warming up")
            output = subprocess.run(cmd, capture_output=True)
            output = subprocess.run(cmd, capture_output=True)
            output = subprocess.run(cmd, capture_output=True)
            print(output.stdout.decode())
            print("Done, starting micromeasurements")
            if args.do_thpt:
                inflight_count=10
                (benchmark_timings, acts) = do_thpt_benchmark("memtest-dirtying-c", count = warm_count, collect_stats=1,
                    duration = 0, inflight_count=inflight_count,
                    numWorkers=1, irq_delay=0, local_client=1)
            else:
                (benchmark_timings, acts) = do_benchmark_activations("memtest-dirtying-c", is_cold=False, count = warm_count, pause = pause_time)

            end_to_end_timnings.extend(benchmark_timings)
            activations.append(acts)
            persist_stats(end_to_end_timnings, limit=len(acts), outdir=outdir, activations = activations,
			suffix="-"+"memtest-dirtying-c-"+"dirty_"+str(args.micro_dirty[i])+"-"+"nmaps_"+str(args.micro_nmaps[i])+"-"+"npages_"+str(args.micro_npages[i]))
        restart_cmd = "./restart-distributed.sh"
        subprocess.run(restart_cmd, shell=True)
        return 0

    #while True:
    #    try:
    #        deployed_benchmarks = [e["name"] for e in do_request(URL_ACTIONS)]
    #        break
    #    except Exception:
    #        restart_cmd = "./restart-distributed.sh"
    #        subprocess.run(restart_cmd, shell=True)

    #for b in all_benchmarks:
    #    if b not in deployed_benchmarks:
    #        do_deploy(b)
    #print(json.dumps(deployed_benchmarks, indent=2, sort_keys=False))
    all_activations_count = 0


    #let's kill all containers
    all_end_to_end_timnings = []
    all_activations = []
    all_thpt_ctivations = []

    for b in args.benchmarks:
        #subprocess.run(["./kill_container.sh"])
        #subprocess.run(["./kill_container.sh"])
        #subprocess.run(["./kill_container.sh"])
        restart_cmd = "./restart-distributed.sh"
        subprocess.run(restart_cmd, shell=True)
        activations_count = 0
        end_to_end_timnings = []
        activations = []
        thpt_ctivations = []

        if args.is_cold:
            #benchmark_timings = do_benchmark(b, is_cold=True, count = cold_count)
            (benchmark_timings, acts) = do_benchmark_activations(b, is_cold=True, count = cold_count, pause = pause_time)
            end_to_end_timnings.extend(benchmark_timings)
            all_end_to_end_timnings.extend(benchmark_timings)
            #acts = get_activations(cold_count)
            activations.append(acts)
            all_activations.append(acts)
            activations_count+=cold_count
            all_activations_count+=cold_count

        if args.is_warm:
            #if pause_time:
            #    (benchmark_timings, acts) = do_benchmark_activations(b, is_cold=False, count = warm_count, pause = pause_time)
            #else:
            (benchmark_timings, acts) = do_thpt_benchmark(b, count = warm_count, collect_stats=1,
                                                duration = 0, inflight_count=inflight_count,
                                                numWorkers=1, irq_delay=pause_time, local_client=1)

            end_to_end_timnings.extend(benchmark_timings)
            all_end_to_end_timnings.extend(benchmark_timings)
            #acts = get_activations(benchmark_activations)
            activations.append(acts)
            all_activations.append(acts)
            activations_count+=warm_count
            all_activations_count+=warm_count

        if args.do_thpt:
            if b in low_latency_benchmarks:
                req_container = 32
            thpt_count = 0

            if args.scale_out:
                print("Warming UP")
                do_thpt_benchmark(b, count = thpt_count, collect_stats=0, duration = 40, inflight_count=4)
                print("Warm UP DONE!")

                subprocess.run(f"./do_mpstats-distributed.sh {outdir}/{b} 0", shell=True)
                (benchmark_timings, acts) = do_thpt_benchmark(b, count = thpt_count, collect_stats=1,
                                                    duration = thpt_duration, inflight_count=req_container*scale_factor)

                subprocess.run(f"./do_mpstats-distributed.sh {outdir}/{b} 1", shell=True)
            #subprocess.run(f"./kill_docker_stats.sh", shell=True)
            #print(benchmark_timings)
            #print(len(benchmark_timings))
            #print(acts)
            #print(len(acts))

            end_to_end_timnings.extend(benchmark_timings)
            all_end_to_end_timnings.extend(benchmark_timings)
            #acts = get_activations(thpt_count)
            activations.append(acts)
            all_activations.append(acts)
            activations_count+=thpt_count
            all_activations_count+=thpt_count

        persist_stats(end_to_end_timnings, limit=activations_count, outdir=outdir, activations = activations, suffix="-"+b)
    persist_stats(all_end_to_end_timnings, limit=all_activations_count, outdir=outdir, activations = all_activations)


    #deployed_benchmarks_params = dict()


if __name__ == "__main__":
    main()

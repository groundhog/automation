# Benchmarks
The benchmarks included in `func`/ are:
- Our Microbenchmark: memtest-dirtying, which sets up a number of memory pages and allows requests to modify (dirty) a subset of them.
- Benchmarks from the FaaSProfiler benchmark suite: https://github.com/PrincetonUniversity/faas-profiler/blob/master/functions
- Benchmarks from the pyperformance and polybench/C benchmark suites (retrieved from Faasm): https://github.com/faasm/experiment-microbench


The FaaSProfiler benchmarks are:
| **Benchmark**          | **Language**    | **Description**                                                                                                                                                                                                                       |
|------------------------|-----------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **get-time**           | Python, NodeJS  | Returns the current timestamp                                                                                                                                                                                                         |
| **base64**             | Python, NodeJS  | Generates a 1million character string, concatenate its base64 encoding 100 times, decodes it and returns the decoded string length. (We added a minimal fix to generate random strings rather than single character repeated strings) |
| **json**               | Python, NodeJS  | Parses a 200KB worth of json x,y,z coordinates and returns the sum of each coordinate                                                                                                                                                 |
| **primes**             | Python, NodeJS  | Finds all primes numbers below 10 million and return their count (using the standard optimized sieve algorithm)                                                                                                                       |
| **autocomplete**       | NodeJS          | Finds a word in a corpus of words using binary search                                                                                                                                                                                 |
| **markdown-to-html**   | Python          | Converts a base64 encoded markdown to HTML using the markdown python package                                                                                                                                                          |
| **img-resize**         | NodeJS          | Resizes an image to several icons using the jimp node js package                                                                                                                                                                      |
| **ocr-img**            | NodeJS + binary | Find text in an image using the Tesseract OCR binary                                                                                                                                                                                  |
| **sentiment-analysis** | Python          | Sentiment analysis of given text                                                                                                                                                                                                      |


The pyperformance benchmarks we use are the same used by faasm: `"pyperf-chaos" "pyperf-genshi" "pyperf-logging" "pyperf-pyaes" "pyperf-spectral_norm" "pyperf-deltablue" "pyperf-go" "pyperf-mdp" "pyperf-pyflate" "pyperf-telco" "pyperf-dulwich" "pyperf-hexiom" "pyperf-nbody" "pyperf-raytrace" "pyperf-unpack_sequence" "pyperf-fannkuch" "pyperf-json_dumps" "pyperf-pickle" "pyperf-richards" "pyperf-version" "pyperf-float" "pyperf-json_loads" "pyperf-pidigits" "pyperf-scimark"`. A detailed description of the benchmarks is available at https://pyperformance.readthedocs.io/benchmarks.html


The polybench benchmarks we use are the same used by faasm: `"polybench-2mm" "polybench-3mm" "polybench-adi" "polybench-atax" "polybench-bicg" "polybench-cholesky" "polybench-correlation" "polybench-covariance" "polybench-deriche" "polybench-doitgen" "polybench-durbin" "polybench-fdtd-2d" "polybench-floyd-warshall" "polybench-gemm" "polybench-gemver" "polybench-gesummv" "polybench-gramschmidt" "polybench-heat-3d" "polybench-jacobi-1d" "polybench-jacobi-2d" "polybench-lu" "polybench-ludcmp" "polybench-mvt" "polybench-nussinov" "polybench-seidel-2d" "polybench-symm" "polybench-syr2k" "polybench-syrk" "polybench-trisolv" "polybench-trmm"`. A detailed description of the benchmarks is available at https://web.cse.ohio-state.edu/~pouchet.2/software/polybench/

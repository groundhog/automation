#!/bin/bash

source configs.env
source cmd.sh

user="ubuntu"
#  do_cmd
#  do_bg=0 ## default value
#  ruser=$1
#  host=$2
#  cmd=$3
#  do_bg=$4
#  out_null=$5
(do_cmd $user $CORE_MACHINE "docker restart controller0")&
(do_cmd $user $INVOKER_MACHINE "docker restart invoker0")&

wait $(jobs -p)
sleep 60

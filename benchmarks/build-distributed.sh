source configs.env
source cmd.sh

user="ubuntu"
benchmarks_dir="/local/workspace/automation/benchmarks"
#  do_cmd
#  do_bg=0 ## default value
#  ruser=$1
#  host=$2
#  cmd=$3
#  do_bg=$4
#  out_null=$5

hostname=`hostname`
hostname_index=`hostname | cut -d"-" -f3`
CORE_MACHINE="$hostname"
INVOKER_MACHINE="ow-invoker0-$hostname_index"
CLIENT_MACHINE="ow-client-$hostname_index"

echo $CORE_MACHINE $INVOKER_MACHINE $CLIENT_MACHINE
echo "maximum number of containers allowed = 1 or 2x#CPUs: $num_containers"

./set_environment.sh $CORE_MACHINE $INVOKER_MACHINE $CLIENT_MACHINE


(do_cmd $user $CORE_MACHINE "cd $benchmarks_dir; git checkout .; git pull; ./set_environment.sh $CORE_MACHINE $INVOKER_MACHINE $CLIENT_MACHINE;./build.sh $@")&
(do_cmd $user $INVOKER_MACHINE "cd $benchmarks_dir; git checkout .; git pull;./set_environment.sh $CORE_MACHINE $INVOKER_MACHINE $CLIENT_MACHINE; ./build.sh $@")&
(do_cmd $user $CLIENT_MACHINE "cd $benchmarks_dir; git checkout .; git pull; ./set_environment.sh $CORE_MACHINE $INVOKER_MACHINE $CLIENT_MACHINE")&

wait $(jobs -p)

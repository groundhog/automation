#!/bin/bash
# File              : kill_container.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 25.10.2020
# Last Modified Date: 10.11.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
fn_name=${1:-"whisk/action"}
echo "killing containers of $fn_name"
docker_containers_list=`sudo docker ps | grep "$fn_name" | awk '{print $1}'`
if [ ! -z "$docker_containers_list" ]
then
	ps aux | grep "$docker_containers_list" | awk '{print $2}' | sudo xargs kill -9
fi
echo "killed all containers of $fn_name"
#fn_name=${1:-"openwhisk/"}
#ps aux | grep "`sudo docker ps | grep "$fn_name" | awk '{print $1}'`" | awk '{print $2}' | sudo xargs kill -9
#fn_name=$1
#ps aux | grep `sudo docker ps | grep $fn_name | awk '{print $1}'` | awk '{print $2}' | sudo xargs kill -9

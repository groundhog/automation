#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# File              : asyncio_client.py
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 04.12.2020
# Last Modified Date: 22.12.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
#https://github.com/flyakite/100-million-requests-aiohttp/blob/master/client.py
import argparse
import asyncio
import sys
import resource
import json
from aiohttp import ClientTimeout, ClientSession, BasicAuth
from functools import partial
import multiprocessing
import itertools
import time
import socket

OW_APIHOST = 'ow-core-' + socket.gethostname().split("-")[-1]
WSK_AUTH = '23bc46b1-71f6-4ed5-8c54-816aa4f8c502:123zO3xZCLrMN6v2BKK1dXYFpXlPkccOFqm12CdAsMgRU4VrNZ9lyGVCGuMDGIwP'
WSK_AUTH_USER, WSK_AUTH_PASS = WSK_AUTH.split(':')
URL_ACTIONS = f'https://{OW_APIHOST}/api/v1/namespaces/guest/actions/'
#param_file_cache = {}   # a cache to keep json of param files

action = ""
param_file = ""
url = ""
auth = BasicAuth(WSK_AUTH_USER, WSK_AUTH_PASS)
param_file_body = ""
concurrent_limit = 48
duration = 0
timeout = 0
start_time = 0
params = {'blocking': 'true', 'result': 'false'}
numWorkers = 2
irq_delay = 0
def get_mem():
    return resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1000

async def run(session, number_of_requests, concurrent_limit):
    global duration
    tasks = []
    responses = []
    sem = asyncio.Semaphore(concurrent_limit)
    i = 0
    start_time = time.time()
    async def fetch(i):
        try:
            #print(timeout)
            #async with session.get(url) as response:
            start_ms = time.time_ns()
            async with session.post(url=url, auth=auth, json=param_file_body, params=params, ssl=False, timeout=timeout) as r:
            #async with session.post(url=url, auth=authentication, json=param_file_body, params=parameters) as response:
                try:
                    #response = await r.json()
                    response = await r.json()
                    end_end_ms = (time.time_ns()) - start_ms
                    response['end_end_latency'] = end_end_ms // 1e6
                    if r.status == 200 and 'activationId' in response:
                        responses.append(response)
                    #if i % 100 == 0:
                    #    print("n:{:10d} tasks:{:10d} {:.1f}MB".format(i, len(tasks), get_mem()))
                    sem.release()
                    return response
                except Exception:
                    sem.release()
                    return {}
        except Exception:
            sem.release()
            return {}


    elapsed_time = time.time() - start_time
    #print (elapsed_time, duration)
    #while (elapsed_time < duration or i < number_of_requests):
    while ((duration > 0 and elapsed_time < duration) or (duration == 0 and i < number_of_requests)) :
        await sem.acquire()
        task = asyncio.ensure_future(fetch(i))
        task.add_done_callback(tasks.remove)
        tasks.append(task)
        elapsed_time = time.time() - start_time
        if irq_delay:
            time.sleep(irq_delay/1000.0)
        i += 1


    await asyncio.wait(tasks)
    #print("total_responses: {}".format(len(responses)))
    return responses

async def send_requests(number_of_requests):
    global duration
    global timeout
    timeout = ClientTimeout(total=300)
    async with ClientSession(timeout=timeout) as session:
        responses = await asyncio.ensure_future(run(session, number_of_requests, concurrent_limit))
        return responses

def worker(number_of_requests):
    loop = asyncio.get_event_loop()
    result = loop.run_until_complete(send_requests(number_of_requests))
    loop.close()
    return result


if __name__ == '__main__':
    """
    run: python asyncio_client.py -n 1000 -c 100 -a get-time-python -p get-time/sample-params.json
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-n')
    parser.add_argument('-c')
    parser.add_argument('-p')
    parser.add_argument('-a')
    parser.add_argument('-d')
    parser.add_argument('-w')
    parser.add_argument('-i')
    args = parser.parse_args()
    number_of_requests = int(args.n) if args.n else 1000
    concurrent_limit = int(args.c) if args.c else 1000
    param_file = args.p
    action = args.a
    duration = int(args.d) if args.d else 0
    if args.w:
        numWorkers = int(args.w)
    irq_delay = float(args.i) if args.i else 0

    if duration == 0:
        numWorkers = 1
    url = URL_ACTIONS + action
    #url = "http://127.0.0.1:8080"
    with open(param_file, 'r') as f:
        param_file_body = json.load(f)

    concurrent_limit = (concurrent_limit/numWorkers)
    #print("number_of_requests: {}, concurrent_limit: {}, action: {}".format(number_of_requests, concurrent_limit, action))
    pool = multiprocessing.Pool(processes = numWorkers)
    #old_time = time.time()
    result = pool.map(worker, [ int(number_of_requests/numWorkers) ] * numWorkers )
    #tot_time = time.time() - old_time
    #print(tot_time)
    #act_ids = list(itertools.chain(*result))
    #act_ids = [next(it) for it in itertools.cycle([iter(l) for l in result])]
    act_ids = list(itertools.chain(*result))
    #for i in range(len(result[0])):
    #    for l in result:
    #        if len(l) > i:
    #            act_ids.append(l[i])

    #print(len(act_ids), "xput: ", len(act_ids)/tot_time)
    #act_ids = [x for x in itertools.chain.from_iterable(itertools.zip_longest(list1, list2)) if x is not None]
    print(json.dumps(act_ids))
    #print(",".join(act_ids))

#!/bin/bash

source configs.env
source cmd.sh

user="ubuntu"
benchmarks_dir="/local/workspace/automation/benchmarks"
#  do_cmd
#  do_bg=0 ## default value
#  ruser=$1
#  host=$2
#  cmd=$3
#  do_bg=$4
#  out_null=$5

do_cmd $user $INVOKER_MACHINE "cd $benchmarks_dir; ./kill_container.sh $@"

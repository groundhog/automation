#!/bin/bash
# File              : build_deploy_and_run.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 23.11.2020
# Last Modified Date: 06.12.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>

do_build=1
instrumentation=0
concurrency=0
img_names=("actionloop-python-v3.7" "action-nodejs-v14" "actionloop-c")
runtimes=("/local/workspace/openwhisk-runtime-python" "/local/workspace/openwhisk-runtime-nodejs" "/local/workspace/openwhisk-runtime-c")
runtimes_repos=("https://gitlab.mpi-sws.org/groundhog/openwhisk-runtime-python.git" "https://gitlab.mpi-sws.org/groundhog/openwhisk-runtime-nodejs.git" "https://gitlab.mpi-sws.org/groundhog/openwhisk-runtime-c.git")
groundhog_dir=( "/local/workspace/openwhisk-runtime-python/core/python3ActionLoop/groundhog/" "/local/workspace/openwhisk-runtime-nodejs/core/nodejs14Action/groundhog/" "/local/workspace/openwhisk-runtime-c/core/cActionLoop/groundhog/" )
build_commands=( "core:python3ActionLoop:distDocker" "core:nodejs14Action:distDocker" "core:cActionLoop:distDocker" )

for i in "${!runtimes[@]}"; do
	if [ ! -d ${runtimes[$i]} ]
	then
		git clone --recurse-submodules ${runtimes_repos[$i]} ${runtimes[$i]}
	fi
done

let i=0
for r in ${runtimes[@]};
do
	cd $r
	echo $r
	git checkout .
	git reset
	git clean -fd
	git checkout groundhog
	git submodule foreach "(git checkout .)"
	git pull origin groundhog --recurse-submodules
	git fetch --all
	cd -
done

while getopts "i:c:brfsnu" opt; do
  case ${opt} in
		i )
			instrumentation=$OPTARG
			echo "instrumentation= $instrumentation"
			;;
		c )
			concurrency=$OPTARG
			echo "concurrency= $concurrency"
			;;
    b )
      echo "baseline"
			if [ $do_build = 1 ]; then
				let i=0
				for r in ${runtimes[@]};
				do
					cd $r
					git reset
					git clean -fd
					git checkout .
					git fetch
					git checkout baseline
					git pull
					(./gradlew ${build_commands[i]}
					docker tag whisk/${img_names[i]}:latest groundhog/${img_names[i]}-baseline:latest
					docker rmi whisk/${img_names[i]}:latest) &
					cd -
					i=$((i+1))
				done
			fi
      ;;
    r )
      echo "baseline-refactored"
			if [ $do_build = 1 ]; then
				let i=0
				for r in ${runtimes[@]};
				do
					cd $r
					git reset
					git clean -fd
					git checkout .
					git fetch
					git checkout baseline-refactored
					git pull
					(./gradlew ${build_commands[i]}
					docker tag whisk/${img_names[i]}:latest groundhog/${img_names[i]}-baseline-refactored:latest
					docker rmi whisk/${img_names[i]}:latest) &
					cd -
					i=$((i+1))
				done
			fi
      ;;

		f )
      echo "forkline"
			if [ $do_build = 1 ]; then
				let i=0
				for r in ${runtimes[@]};
				do
					cd $r
					git reset
					git clean -fd
					git checkout .
					git fetch
					git pull
					git checkout forkline
					git pull
					(./gradlew ${build_commands[i]}
					docker tag whisk/${img_names[i]}:latest groundhog/${img_names[i]}-forkline:latest
					docker rmi whisk/${img_names[i]}:latest) &
					cd -
					i=$((i+1))
				done
			fi
      ;;
    s )
      echo "groundhog-sd"
			if [ $do_build = 1 ]; then
				let i=0
				for r in ${groundhog_dir[@]};
				do
					cd $r
					git reset
					git clean -fd
					git checkout .
					git fetch
					git checkout main
					git pull
					cp src/config.h.sd src/config.h
					if [ $instrumentation = 0 ]; then
						sed -i 's/XSTATS_CONFIG_STAT 1/XSTATS_CONFIG_STAT 0/g' src/config.h
					fi
					if [ $concurrency -gt 1 ]; then
						sed -i "s/CONCURRENCY 1/CONCURRENCY $concurrency/g" src/config.h
					fi
					# make will happen in the container
					#make
					cd -
					cd ${runtimes[i]}
					git reset
					git clean -fd
					git checkout .
					git fetch
					git checkout groundhog
					git pull
					(./gradlew ${build_commands[i]}
					docker tag whisk/${img_names[i]}:latest groundhog/${img_names[i]}-sd:latest
					docker rmi whisk/${img_names[i]}:latest) &
					cd -
					i=$((i+1))
				done
			fi
      ;;
    n )
      echo "groundhog-nop"
			if [ $do_build = 1 ]; then
				let i=0
				for r in ${groundhog_dir[@]};
				do
					cd $r
					git reset
					git clean -fd
					git checkout .
					git fetch
					git checkout main
					git pull
					cp src/config.h.sd src/config.h
					sed -i 's/GH_NOP 0/GH_NOP 1/g' src/config.h
					if [ $instrumentation = 0 ]; then
						sed -i 's/XSTATS_CONFIG_STAT 1/XSTATS_CONFIG_STAT 0/g' src/config.h
					fi
					# make will happen in the container
					#make
					cd -
					cd ${runtimes[i]}
					git reset
					git clean -fd
					git checkout .
					git fetch
					git checkout groundhog
					git pull
					(./gradlew ${build_commands[i]}
					docker tag whisk/${img_names[i]}:latest groundhog/${img_names[i]}-nop:latest
					docker rmi whisk/${img_names[i]}:latest) &
					cd -
					i=$((i+1))
				done
			fi
      ;;

   \? )
     echo "Invalid Option: -$OPTARG" 1>&2
     ;;
  esac
done

wait $(jobs -p)

#!/bin/bash
source cmd.sh
source configs.env

controller_user=$1
controller_ip=$2
dir_to=$3

latest_dir=$(ls -td /local/workspace/automation/benchmarks/benchmarks/* | head -1 )
rsync -e "ssh $SSH_EXTRA_ARGS" -azP $latest_dir $controller_user@$controller_ip:$dir_to

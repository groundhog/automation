#!/bin/bash
# File              : eval-async.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 15.10.2020
# Last Modified Date: 06.12.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
user="ubuntu"
source cmd.sh
source configs.env

action=$1
param_file=$2
iters=$3
duration=$4
inflight=$5
local_client=$6
numWorkers=$7
irq_delay=$8
#echo $iters
#for i in `seq 1 $iters`; do
#	(for j in `seq 1 20`; do	wsk -i action invoke $action --param-file $param_file; done )&
#done
#for i in `seq 1 $iters`; do
#	(wsk -i action invoke $action --param-file $param_file) &
#done
if [ "$local_client" -eq "0" ];
then
	ssh $SSH_EXTRA_ARGS $user@$CLIENT_MACHINE "cd /local/workspace/automation/benchmarks && python3 asyncio_client.py -d $duration -n $iters -c $inflight -a $action -p $param_file -w $numWorkers -i $irq_delay"
else
	ssh $SSH_EXTRA_ARGS $user@127.0.0.1 "cd /local/workspace/automation/benchmarks && python3 asyncio_client.py -d $duration -n $iters -c $inflight -a $action -p $param_file -w $numWorkers -i $irq_delay"
fi

#wait

#!/bin/bash

#pass 1 to build only
benchmarks=("get-time" "base64" "json" "primes" "autocomplete" "markdown-to-html" "img-resize" "ocr-img" "sentiment-analysis" "pyperf-chaos" "pyperf-genshi" "pyperf-logging" "pyperf-pyaes" "pyperf-spectral_norm" "pyperf-deltablue" "pyperf-go" "pyperf-mdp" "pyperf-pyflate" "pyperf-telco" "pyperf-dulwich" "pyperf-hexiom" "pyperf-nbody" "pyperf-raytrace" "pyperf-unpack_sequence" "pyperf-fannkuch" "pyperf-json_dumps" "pyperf-pickle" "pyperf-richards" "pyperf-version" "pyperf-float" "pyperf-json_loads" "pyperf-pidigits" "pyperf-scimark" "polybench-2mm" "polybench-3mm" "polybench-adi" "polybench-atax" "polybench-bicg" "polybench-cholesky" "polybench-correlation" "polybench-covariance" "polybench-deriche" "polybench-doitgen" "polybench-durbin" "polybench-fdtd-2d" "polybench-floyd-warshall" "polybench-gemm" "polybench-gemver" "polybench-gesummv" "polybench-gramschmidt" "polybench-heat-3d" "polybench-jacobi-1d" "polybench-jacobi-2d" "polybench-lu" "polybench-ludcmp" "polybench-mvt" "polybench-nussinov" "polybench-seidel-2d" "polybench-symm" "polybench-syr2k" "polybench-syrk" "polybench-trisolv" "polybench-trmm" "memtest-dirtying")

for b in ${benchmarks[@]}
do
	echo $b | sed 's|\(.*\)-.*|\1|'
	cd func/$(echo $b | sed 's|\(.*\)-.*|\1|')
	(./deploy.sh $@)
	cd ../../
done

#wait $(jobs -p)

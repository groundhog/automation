#!/bin/bash
source cmd.sh
source configs.env

do_cmd root $CLIENT_MACHINE "shutdown -h now"
do_cmd root $INVOKER_MACHINE "shutdown -h now"
sudo shutdown -h now

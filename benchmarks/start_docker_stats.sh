#!/bin/bash
# File              : start_docker_stats.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 05.12.2020
# Last Modified Date: 06.12.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
file=$1
while true ; do docker stats --no-stream | ts '[%Y-%m-%d %H:%M:%S]' && sleep 1;  done

#!/bin/bash
# File              : eval.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 15.10.2020
# Last Modified Date: 25.10.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
action=$1
param_file=$2

wsk -i action invoke $action --param-file $param_file --blocking | tail -n +2 #--result

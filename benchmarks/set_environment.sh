#!/bin/bash
CORE_MACHINE=${1:-"ow-core"}
INVOKER_MACHINE=${2:-"ow-invoker0"}
CLIENT_MACHINE=${3:-"ow-client"}

sed -i "s/CORE_MACHINE.*/CORE_MACHINE=\"$CORE_MACHINE\"/g" /local/workspace/automation/benchmarks/configs.env
sed -i "s/INVOKER_MACHINE.*/INVOKER_MACHINE=\"$INVOKER_MACHINE\"/g" /local/workspace/automation/benchmarks/configs.env
sed -i "s/CLIENT_MACHINE.*/CLIENT_MACHINE=\"$CLIENT_MACHINE\"/g" /local/workspace/automation/benchmarks/configs.env

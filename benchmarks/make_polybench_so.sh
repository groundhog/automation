cd func;

for d in polybench-*;
do clang-13 -shared -fPIC -O3 -DNDEBUG -I../polybench-preparation/utilities -o $d/`basename $d`.so.originalclang ../polybench-preparation/utilities/polybench.c $d/*.c;
#do clang-13 -shared -fPIC -O3 -mllvm -polly -mllvm -polly-vectorizer=polly -I../polybench-preparation/utilities -o $d/`basename $d`.so.originalclang ../polybench-preparation/utilities/polybench.c $d/*.c;
#do gcc -fPIC -O3 -shared -DDATA_TYPE_IS_FLOAT=1 -ldl -lm -I../polybench-preparation/utilities -o $d/`basename $d`.so.originalclang ../polybench-preparation/utilities/polybench.c $d/*.c;
done

for d in polybench-*; do base64 $d/$d.so.originalclang > $d/$d.so; done

cd -

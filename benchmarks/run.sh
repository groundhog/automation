#!/bin/bash
# File              : run.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 26.10.2020
# Last Modified Date: 06.12.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
parent_dir=$1
tag=${2:-"test"}
config=${3:-"--thpt"}

user="ubuntu"
host="localhost"
source cmd.sh
source configs.env

exptime=$(date +"%Y_%m_%d-%H_%M_%S")
srcdir="/local/workspace/automation/benchmarks/"
statsdir="$parent_dir/$exptime"
mkdir -p ${statsdir}_$tag
do_cmd $user $INVOKER_MACHINE "cd $srcdir; mkdir -p ${statsdir}_$tag"

get_gitinfo $host $srcdir ${statsdir}_$tag "automation"
get_gitinfo $INVOKER_MACHINE $srcdir ${statsdir}_$tag "automation_invoker"

#start_mpstat $ruser $host ${statsdir}_$tag/mpstat.out
#python3 benchmark.py --out ${statsdir}_$tag --thpt --benchmark base64-node
#python3 benchmark.py --out ${statsdir}_$tag --cold --warm --thpt --benchmark get-time-pytho#n
#python3 benchmark.py --out ${statsdir}_$tag --thpt --benchmark get-time-python sentiment-analysis-python base64-node primes-node ocr-img-node img-resize-node primes-python
#python3 benchmark.py --out ${statsdir}_$tag --thpt --benchmark get-time-python
#python3 benchmark.py --out ${statsdir}_$tag --cold --warm
#python3 benchmark.py --out ${statsdir}_$tag --thpt
if [ -n "$4" ]; then
		python3 -u benchmark.py --out ${statsdir}_$tag $config --benchmark $4
	else
		python3 -u benchmark.py --out ${statsdir}_$tag $config
fi

scp -r $SSH_EXTRA_ARGS $user@$INVOKER_MACHINE:${statsdir}_$tag ${statsdir}_$tag/invoker_stats
#stop_mpstat $ruser $host

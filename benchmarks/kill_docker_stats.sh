#!/bin/bash
# File              : kill_docker_stats.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 05.12.2020
# Last Modified Date: 05.12.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
ps aux | grep "bash -c [w]hile true" | head -1 | tr -s " " |  cut -d" " -f2 | sudo xargs kill -9
sudo pkill -f "start_docker_stats.sh"
sudo pkill -f "stream"
ps aux | grep "bash -c [w]hile true" | head -1 | tr -s " " |  cut -d" " -f2 | sudo xargs kill -9
sudo pkill -f "start_docker_stats.sh"
sudo pkill -f "stream"

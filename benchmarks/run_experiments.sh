#!/bin/bash
source cmd.sh
source configs.env
exptime=$(date +"%Y_%m_%d-%H_%M_%S")
owdir="/local/workspace/openwhisk/"
stats_parent_dir="/local/workspace/automation/benchmarks/benchmarks/$exptime"
automation_dir="/local/workspace/automation/benchmarks/"
user="ubuntu"
ruser="root"

#Allow sshing as root for mpstats
sudo cp /home/ubuntu/.ssh/* /root/.ssh/
do_cmd "ubuntu" $INVOKER_MACHINE "sudo cp /home/ubuntu/.ssh/* /root/.ssh/"
do_cmd "ubuntu" $CLIENT_MACHINE "sudo cp /home/ubuntu/.ssh/* /root/.ssh/"


if [[ $1 == *"-h"* ]]; then
	echo "usage: ./run_experiments.sh [<experiment_type> <count> <instrumentation> <run_config> <benchmarks>]"
	echo 'e.g: : ./run_experiments.sh "baseline groundhog" 1 0 "--thpt" ""'
	exit 1
fi
experiment_type=${1:-"groundhog"}
count=${2:-1}
instrumentation=${3:-"0"}
run_config=${4:-""}
benchmarks=${5:-""}
gh_cpus=${6:-0}
number_containers=${7:-$(do_cmd "ubuntu" $INVOKER_MACHINE "nproc")}
first_run=${8:-0}

mkdir -p ${stats_parent_dir}
do_cmd $user $INVOKER_MACHINE "mkdir -p ${stats_parent_dir}"
do_cmd $user $CLIENT_MACHINE "cd $automation_dir; git checkout .; git pull"

do_cmd root $CORE_MACHINE "echo 1 > /proc/sys/vm/unprivileged_userfaultfd"
do_cmd root $INVOKER_MACHINE "echo 1 > /proc/sys/vm/unprivileged_userfaultfd"


echo "experiment_type= $experiment_type, count= $count, instrumentation=$instrumentation, run_config= $run_config, benchmarks=$benchmarks, gh_cpus=$gh_cpus, num_containers=$number_containers" >  $stats_parent_dir/run_experiment_configs.txt

if [[ -n $first_run ]]; then
	if [[ "$run_config" =~ "--scale_out" ]]; then
		./configure-openwhisk.sh $(($number_containers)) > $stats_parent_dir/run_experiment_openwhisk_configs-scale_out.log 2>&1
	else
		./configure-openwhisk.sh 1 > $stats_parent_dir/run_experiment_openwhisk_configs-single_container.log 2>&1
	fi
fi

get_gitinfo $CORE_MACHINE $owdir ${stats_parent_dir} "openwhisk"
get_gitinfo $INVOKER_MACHINE $owdir ${stats_parent_dir} "openwhisk-invoker"

function clear_logs()
{
	echo "clearing logs"
	do_cmd root $CORE_MACHINE "echo \"\" > /var/tmp/wsklogs/controller0/controller0_logs.log"
	do_cmd root $CORE_MACHINE "echo \"\" > /var/tmp/wsklogs/nginx/nginx_access.log"
  do_cmd root $INVOKER_MACHINE "echo \"\" >  /var/tmp/wsklogs/invoker0/invoker0_logs.log"
	./deploy_openwhisk.sh 1 1
}


if [[ "$experiment_type" =~ "baseline" ]]; then
	echo "Baseline"
	clear_logs
	./build-distributed.sh "-i$instrumentation -b" >  $stats_parent_dir/run_experiment_build-baseline.log 2>&1
	./set_benchmarks.sh "$benchmarks"
	./deploy-distributed.sh -b >  $stats_parent_dir/run_experiment_deploy-baseline.log 2>&1
	for i in `seq 1 $count`;
	do
		./run.sh $stats_parent_dir "baseline" "$run_config" "$benchmarks" >> $stats_parent_dir/run_experiment_run-baseline.log  2>&1
	done
fi

if [[ "$experiment_type" =~ "baseline-refactored" ]]; then
	echo "Baseline-refactored"
	clear_logs
	./build-distributed.sh "-i$instrumentation -r" >  $stats_parent_dir/run_experiment_build-baseline-refactored.log 2>&1
	./set_benchmarks.sh "$benchmarks"
	./deploy-distributed.sh -r >  $stats_parent_dir/run_experiment_deploy-baseline-refactored.log 2>&1
	for i in `seq 1 $count`;
	do
		./run.sh $stats_parent_dir "baseline-refactored" "$run_config" "$benchmarks" >> $stats_parent_dir/run_experiment_run-baseline.log  2>&1
	done
fi

if [[ "$experiment_type" =~ "forkline" ]]; then
	echo "Forkline"
	clear_logs
	./build-distributed.sh "-i$instrumentation -f" >  $stats_parent_dir/run_experiment_build-forkline.log 2>&1
	./set_benchmarks.sh "$benchmarks"
	./deploy-distributed.sh -f >  $stats_parent_dir/run_experiment_deploy-forkline.log 2>&1
	for i in `seq 1 $count`;
	do
		./run.sh $stats_parent_dir "forkline" "$run_config" "$benchmarks" >> $stats_parent_dir/run_experiment_run-forkline.log  2>&1
	done
fi


if [[ "$experiment_type" =~ "groundhog-sd" ]]; then
	echo "Groundhog"
	clear_logs
	./build-distributed.sh "-i$instrumentation -c$gh_cpus -s" >  $stats_parent_dir/run_experiment_build-groundhogSD.log  2>&1
	./set_benchmarks.sh "$benchmarks"
	./deploy-distributed.sh -s > $stats_parent_dir/run_experiment_deploy-groundhogSD.log  2>&1
	for i in `seq 1 $count`;
	do
		./run.sh $stats_parent_dir "groundhog-sd" "$run_config" "$benchmarks" >>  $stats_parent_dir/run_experiment_run-groundhogSD.log 2>&1
	done
fi

if [[ "$experiment_type" =~ "gh-nop" ]]; then
	echo "Groundhog-NOP"
	clear_logs
	./build-distributed.sh "-i$instrumentation -n" >  $stats_parent_dir/run_experiment_build-groundhogNOP.log  2>&1
	./set_benchmarks.sh "$benchmarks"
	./deploy-distributed.sh -n > $stats_parent_dir/run_experiment_deploy-groundhogNOP.log  2>&1
	for i in `seq 1 $count`;
	do
		./run.sh $stats_parent_dir "gh-nop" "$run_config" "$benchmarks" >>  $stats_parent_dir/run_experiment_run-groundhogNOP.log 2>&1
	done
fi

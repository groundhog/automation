source cmd.sh
do_deploy=1
img_names=("actionloop-python-v3.7" "action-nodejs-v14" "actionloop-c")

while getopts brfsnu opt; do
  case ${opt} in
    b )
      echo "baseline"
			if [ $do_deploy = 1 ]; then
				for r in ${img_names[@]};
				do
					docker tag groundhog/$r-baseline:latest whisk/$r:latest
				done
				./deploy_benchmarks.sh
				./deploy_benchmarks.sh
			fi
      ;;
    r )
      echo "baseline-refactored"
			if [ $do_deploy = 1 ]; then
				for r in ${img_names[@]};
				do
					docker tag groundhog/$r-baseline-refactored:latest whisk/$r:latest
				done
				./deploy_benchmarks.sh
				./deploy_benchmarks.sh
			fi
      ;;
    f )
      echo "forkline"
			if [ $do_deploy = 1 ]; then
				for r in ${img_names[@]};
				do
					docker tag groundhog/$r-forkline:latest whisk/$r:latest
				done
				./deploy_benchmarks.sh
				./deploy_benchmarks.sh
			fi
      ;;
    s )
      echo "groundhog-sd"
			if [ $do_deploy = 1 ]; then
				for r in ${img_names[@]};
				do
					docker tag groundhog/$r-sd:latest whisk/$r:latest
				done
				./deploy_benchmarks.sh
				./deploy_benchmarks.sh
			fi
      ;;
    n )
      echo "groundhog-nop"
			if [ $do_deploy = 1 ]; then
				for r in ${img_names[@]};
				do
					docker tag groundhog/$r-nop:latest whisk/$r:latest
				done
				./deploy_benchmarks.sh
				./deploy_benchmarks.sh
			fi
      ;;

   \? )
     echo "Invalid Option: -$OPTARG" 1>&2
     ;;
  esac
done

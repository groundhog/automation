#!/bin/bash
# File              : deploy.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 15.10.2020
# Last Modified Date: 23.11.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>

#sudo add-apt-repository -y ppa:deadsnakes/ppa
#sudo apt-get update
#sudo apt-get -y install python3.6
#virtualenv -p /usr/bin/python3.6 virtualenv
#./virtualenv/bin/pip install textblob
#./virtualenv/bin/pip install importlib-metadata
#zip -r sentiment-analysis.zip virtualenv __main__.py
#wsk -i action update sentiment-analysis-python --kind python:3 sentiment-analysis.zip
#wsk -i action update sentiment-analysis-python sentiment.py --docker immortalfaas/sentiment
source ../../configs.env
#$CONFIGS --docker $IMG_REPO/$PYTHON_IMG
docker build --tag  whisk/actionloop-python-v3.7-sentiment:latest .
build_only=${1:-0}

if [[ "$build_only" != 0 ]]; then
	exit 0
fi
wsk -i action update sentiment-analysis-python sentiment.py $CONFIGS --docker whisk/actionloop-python-v3.7-sentiment:latest

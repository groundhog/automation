import os
from os.path import exists

import dulwich.repo


def main(params):
    repo_path = os.path.dirname(os.__file__) +"/site-packages/pyperformance/benchmarks/data/asyncio.git"
    repo = dulwich.repo.Repo(repo_path)
    head = repo.head()

    # Iterate on all changes on the Git repository
    for _ in repo.get_walker(head):
        pass

    repo.close()




    return {}

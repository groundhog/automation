from pyperformance.benchmarks.bm_raytrace import (
    bench_raytrace,
    DEFAULT_WIDTH,
    DEFAULT_HEIGHT,
)


def main(params):
    filename = "raytrace.ppm"
    bench_raytrace(2, DEFAULT_WIDTH, DEFAULT_HEIGHT, filename)



    
    return {}

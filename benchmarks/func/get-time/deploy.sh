#!/bin/bash
# File              : deploy.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 15.10.2020
# Last Modified Date: 24.11.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
source ../../configs.env
build_only=${1:-0}

if [[ "$build_only" != 0 ]]; then
	exit 0
fi
arr=("py" "js")

for file in *
do
    if [[ -f $file ]]; then
			if echo ${arr[@]} | grep -q -w "${file##*.}";
			then
				case "${file##*.}" in
					"py") IMG=$PYTHON_IMG ;;
					"js") IMG=$NODE_IMG ;;
				esac
				echo "wsk -i action update ${file%.*} $file $CONFIGS --docker $IMG_REPO/$IMG"
				wsk -i action update ${file%.*} $file $CONFIGS --docker $IMG_REPO/$IMG
			fi
    fi
done

#deploy C
#sudo docker run -it -v $PWD:/action/ -w /action/ openwhisk/dockerskeleton ./build.sh
#zip -r myAction.zip exec
#wsk -i action update get-time-c myAction.zip --native

/**
 * File              : get-time-c.c
 * Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
 * Date              : 18.10.2020
 * Last Modified Date: 19.10.2020
 * Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
 */
#include <stdio.h>
#include <time.h>
#include <string.h>

int count_invocations = 0;

int main ()
{
  time_t rawtime;
  struct tm * timeinfo;

  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
	char* tstr = asctime(timeinfo);
	tstr[strlen(tstr) - 1] = 0;
	printf ( "{ \"result\" : \"Invocation# %d, Crurrent local time and date: %s\" }\n", count_invocations++, tstr );

  return 0;
}

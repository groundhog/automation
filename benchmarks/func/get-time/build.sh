#!/bin/bash
# File              : build.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 19.10.2020
# Last Modified Date: 19.10.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
#!/bin/bash
apk add gcc libc-dev
gcc get-time-c.c -o exec

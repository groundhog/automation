#!/bin/bash
# File              : deploy.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 15.10.2020
# Last Modified Date: 10.11.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
source ../../configs.env
build_only=${1:-0}

if [[ "$build_only" != 0 ]]; then
	exit 0
fi
#sudo add-apt-repository -y ppa:deadsnakes/ppa
#sudo apt-get update
#sudo apt-get -y install python3.6
#virtualenv -p /usr/bin/python3.6 virtualenv
#./virtualenv/bin/pip install markdown
#./virtualenv/bin/pip install importlib-metadata
#zip -r markdown2html.zip virtualenv __main__.py
#wsk -i action update markdown-to-html-python --kind python:3 markdown2html.zip
echo "wsk -i action update markdown-to-html-python markdown2html.zip $CONFIGS --docker $IMG_REPO/$PYTHON_IMG"
wsk -i action update markdown-to-html-python markdown2html.zip $CONFIGS --docker $IMG_REPO/$PYTHON_IMG

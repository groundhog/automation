#!/bin/bash
# File              : deploy.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 15.10.2020
# Last Modified Date: 23.11.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
docker build --tag whisk/action-nodejs-v14-ocr:latest .
source ../../configs.env
build_only=${1:-0}

if [[ "$build_only" != 0 ]]; then
	exit 0
fi


npm install
zip -r ocr-img.zip *
#wsk -i action update ocr-img-node handler.js --docker immortalfaas/nodejs-tesseract
wsk -i action update ocr-img-node handler.js $CONFIGS --docker whisk/action-nodejs-v14-ocr:latest

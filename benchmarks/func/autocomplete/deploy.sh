#!/bin/bash
# File              : deploy.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 16.10.2020
# Last Modified Date: 10.11.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
source ../../configs.env
build_only=${1:-0}

if [[ "$build_only" != 0 ]]; then
	exit 0
fi

case "$IMG_REPO" in
	"openwhisk") SECURE=0 ;;
	"whisk") SECURE=1 ;;
esac

sudo npm link
node bin/acsetup.js data/all.txt 1
#$SECURE

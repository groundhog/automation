/**
 * @file             : whisk.js
 * @author           : Mohamed Alzayat <alzayat@mpi-sws.org>
 * Date              : 16.10.2020
 * Last Modified Date: 10.11.2020
 * Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
 */
const child_process = require('child_process');
const spawn = child_process.spawnSync;
const colors = require('colors');
fs = require('fs');

// get the openwhisk namespace
const namespace = function() {
  // get the names space that wsk is publishing to
  var ns = spawn( 'wsk', ['namespace', 'list', '-i']);
  var str = ns.stdout.toString('utf8');
  var bits = str.split('\n');
  var retval = null;
  if (bits.length >= 2 && bits[0] === 'namespaces') {
    retval = bits[1];
  }
  return retval.trim();
};

// deploy an action
const createAction = function(name, path, groundhog) {

  // create package
  var packageParams = ['package', 'update', 'autocomplete', '-i'];
  var packageCreate = spawn( 'wsk', packageParams);

  if(packageCreate.status) {
    // package could not be created/updated; display error and abort processing
    console.error(packageCreate.stderr.toString('utf8').red);
    return false;
  }

  // create OpenWhisk action
  // I5: apply workaround for server-side issue
	//var createParams = ['action', 'update', 'autocomplete-node', '--kind', 'nodejs:10', path, '-i'];
	//if (groundhog == 1){
  var createParams = ['-i', 'action', 'update', 'autocomplete-node', path, '-m', '2048', '--docker', 'whisk/action-nodejs-v14:latest'];
	//}
	//else{
  //	var createParams = ['-i', 'action', 'update', 'autocomplete-node', path, '--docker', 'openwhisk/action-nodejs-v14:latest'];
	//}
	console.log(createParams);
  var actionCreate = spawn( 'wsk', createParams);

  if(actionCreate.status) {
    // action could not be created/updated; display error and abort processing
    console.error(actionCreate.stderr.toString('utf8').red);
    return false;
  }

  return true;
};

const url = function(namespace, package, action) {
  return 'https://localhost/api/v1/web/' + namespace + '/' + package + '-' + action + "-node";
}


module.exports = {
  namespace: namespace,
  createAction: createAction,
  url: url
};

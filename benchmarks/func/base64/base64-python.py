# File              : base64-python.py
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 15.10.2020
# Last Modified Date: 13.11.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
# Copyright (c) 2019 Princeton University
# Copyright (c) 2014 'Konstantin Makarchev'
#
# This source code is licensed under the MIT license found in the
# LICENSE file in the root directory of this source tree.

import base64
import string
import random

def main(params):
    STR_SIZE = params["str_size"]
    TRIES = params["tries"]
    #str1 = bytes(random.choice(string.ascii_letters), encoding="ascii") * STR_SIZE
    str1 = bytes(''.join(random.choices(string.ascii_letters, k=STR_SIZE)), encoding="ascii")
    str2 = b""
    s_encode = 0
    for _ in range(0, TRIES):
        str2 = base64.b64encode(str1)
        s_encode += len(str2)

    s_decode = 0
    for _ in range(0, TRIES):
        s_decode += len(base64.b64decode(str2))

    result = {'s_encode' : str(s_encode), 's_decode' : str(s_decode)}

    return result

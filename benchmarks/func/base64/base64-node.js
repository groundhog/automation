/**
 * @file             : base64-node.js
 * @author           : Mohamed Alzayat <alzayat@mpi-sws.org>
 * Date              : 15.10.2020
 * Last Modified Date: 16.10.2020
 * Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
 */
/*
 Copyright (c) 2019 Princeton University
 Copyright (c) 2014 'Konstantin Makarchev'

 This source code is licensed under the MIT license found in the
 LICENSE file in the root directory of this source tree.
*/

'use strict';

function main(params) {

    var STR_SIZE = params.str_size;
    var TRIES = params.tries;
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	  var charactersLength = characters.length;
    var str = ""; for (var i = 0; i < STR_SIZE; i++) str +=
					characters.charAt(Math.floor(Math.random() * charactersLength));
    var str2 = "";

    var s_encode = 0;
    for (var i = 0; i < TRIES; i++) {
        var b = new Buffer.from(str);
        str2 = b.toString('base64');
        s_encode += str2.length;
    }

    var s_decode = 0;
    for (var i = 0; i < TRIES; i++) {
        var b = new Buffer.from(str2, 'base64');
        var str3 = b.toString();
        s_decode += str3.length;
    }

  return { 's_encode': s_encode.toString(), 's_decode': s_decode.toString() };
}

module.exports.main = main;

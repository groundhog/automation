/**
 * File              : memtest-diff.c
 * Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
 * Date              : 22.04.2020
 * Last Modified Date: 16.11.2020
 * Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
 */
#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/mman.h>
#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <execinfo.h>
#include <signal.h>
#include <time.h>
#include <cJSON.h>

#define PSIZE 4096
int NPAGES;

void handler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}



static inline int create_mmap_shared(char **buf_shared, int map_size){
	char mmap_file[BUFSIZ];
	// Create a new shared memory region (file) and mmap it
	sprintf(mmap_file, "/tmpshmem");
	shm_unlink(mmap_file);
	int fd = shm_open(mmap_file, O_CREAT | O_RDWR, 0666);
	assert(fd > 0);
	int r = ftruncate(fd, map_size);
	assert(r == 0);

	*buf_shared = (char*) mmap(NULL, map_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

	if ((*buf_shared) == MAP_FAILED) {
		printf("mmap failed: %s\n", strerror(errno));
		close(fd);
		return -1;
	}

	//Force demand paging NOW!
	for (int i = 0; i < map_size; i+=PSIZE){
		((char*)(*buf_shared))[i] = i;
	}
	return fd;
}

static inline int mmap_private_from_shared(int shm_fd, char**buf_cow, int map_size){
	*buf_cow = mmap(NULL, map_size, PROT_READ | PROT_WRITE,
			MAP_PRIVATE, shm_fd, 0);
	if ((*buf_cow) == MAP_FAILED) {
		printf("mmap failed: %s\n", strerror(errno));
		return -1;
	}
	return shm_fd;
}

static inline void clean_mmap(char *buf, int map_size){
	int r = munmap( buf, map_size );
	if (r == -1 ) {
		printf("munmap failed with error %d", r);
	}
}

static inline void clean_mmap_shared(int fd, char *buf, int map_size){
	char mmap_file[BUFSIZ];
	sprintf(mmap_file, "/tmpshmem");
	int r = munmap( buf, map_size );
	if (r == -1 ) {
		printf("munmap failed with error %d", r);
	}
	shm_unlink(mmap_file);
	close(fd);
}

static inline void write_random_pages_uniform(char* buf, int num_pages_to_dirty){
	uint64_t buffer_index=-1;
	for (int i = 0 ; i < num_pages_to_dirty; i++){
		buffer_index = (i * (uint64_t)NPAGES/num_pages_to_dirty)*PSIZE;
		buf[buffer_index] = i*7;
	}
}

static inline int count_dirtied_pages(char* buf, int num_pages, char* reference_memory){
	int count =0;
	for (int i = 0 ; i < num_pages; i++){
		//if (memcmp(buf + i*PSIZE, reference_memory + i*PSIZE, PSIZE) != 0)
		//	count ++;
		count += ((int*)(buf + i*PSIZE))[0];
	}
	return count;
}


char *allocate_random_heap_buffer(size_t size) {
    time_t current_time = time(NULL);
    srandom((unsigned int) current_time);
    char* allocatedMemory = (char *) malloc(size);

    for (int bufferIndex = 0; bufferIndex < size; bufferIndex++) {
        uint8_t randomNumber = (uint8_t) random();
        allocatedMemory[bufferIndex] = randomNumber;
    }

    return allocatedMemory;
}

char* randomblock;
char** memory_maps;
int c = 0;
int setup(int percentage, int nmaps){

	int map_size = NPAGES*PSIZE ;
	char *buf_shared;
	char *buf_private_cow;
	void* base_addr = (void*) 0x600000000000;

	//char randomblock [100*PSIZE];
	randomblock = allocate_random_heap_buffer(NPAGES*PSIZE);
	//memset (randomblock, 0, 100*PSIZE);

	memory_maps = (char**) malloc(sizeof(char*) * nmaps);
	for (int i = 0; i < nmaps; i++){
		memory_maps[i] = mmap(base_addr + (i*NPAGES*PSIZE + i*PSIZE), NPAGES*PSIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
		//mmap((void*) 0x7f3633400000, 32*PSIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED , -1, 0);
		memcpy(memory_maps[i], randomblock, NPAGES*PSIZE);
	}
	c += 1;
	return 0;
}

char* func(char* argv) {
  //signal(SIGSEGV, handler);   // install our handler
	//printf("Welcome to the memory dirtying test case\n");
	//fflush(stdout);

	//if (argc < 4)
	//	printf("Uage: ./memtest-dirtying <dirtying percentage> <number of maps> <map size>\n");
	cJSON *req_json = cJSON_Parse(argv);
	if (req_json == NULL)
    {
        const char *error_ptr = cJSON_GetErrorPtr();
        if (error_ptr != NULL)
        {
            fprintf(stderr, "Error before: %s\n", error_ptr);
						fflush(stderr);
        }
    }
 	cJSON *perc_arg = cJSON_GetObjectItemCaseSensitive(req_json,"percentage");
 	cJSON *nmaps_arg = cJSON_GetObjectItemCaseSensitive(req_json,"nmaps");
 	cJSON *npages_arg = cJSON_GetObjectItemCaseSensitive(req_json,"NPAGES");
	int percentage = perc_arg->valueint;
	int nmaps = nmaps_arg->valueint;
	NPAGES = npages_arg->valueint;
	if (c == 0)
		setup(percentage, nmaps);

	int before_count_dirtied = 0;
	//for (int i = 0; i < nmaps; i++){
	//	before_count_dirtied += count_dirtied_pages(memory_maps[i], NPAGES, randomblock);
	//}

	for (int i = 0; i < nmaps && c > 1; i++){
		write_random_pages_uniform(memory_maps[i], (int) ((percentage * NPAGES ) / 100.0));
	}

	int count_dirtied = 0;
	for (int i = 0; i < nmaps; i++){
		count_dirtied += count_dirtied_pages(memory_maps[i], NPAGES, randomblock);
	}
	c++;
//printf("Number of dirtied pages: %d, out of %d = %f percent", count_dirtied, nmaps*NPAGES, 100*(((float) count_dirtied)/ (nmaps*NPAGES)));
	//fflush(stdout);

	//printf("\n=========%d================================\n", c++);
	//fflush(stdout);

	char* ret = malloc(100);
	sprintf(ret, "{\"before_dirty_pages\":%d, \"num_dirty_pages\":%d, \"num_pages\":%d, \"percent\":%f}",
			before_count_dirtied,count_dirtied, nmaps*NPAGES, 100*(((float) count_dirtied)/ (nmaps*NPAGES)) );
	return ret;
}

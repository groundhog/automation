#!/bin/bash
# File              : deploy.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 15.10.2020
# Last Modified Date: 24.11.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
source ../../configs.env
build_only=${1:-0}

if [[ "$build_only" != 0 ]]; then
	exit 0
fi

#clang-13 -shared -fPIC -O0 -DDATA_TYPE_IS_FLOAT=1 -I. -o memtest-dirtying.so.originalclang memtest-dirtying.c cJSON.c
#base64 memtest-dirtying.so.originalclang > memtest-dirtying.so

IMG=$C_IMG
file='memtest-dirtying.so'

echo "wsk -i action update ${file%.*}-c $file $CONFIGS --docker $IMG_REPO/$IMG"
wsk -i action update ${file%.*}-c $file $CONFIGS --docker $IMG_REPO/$IMG

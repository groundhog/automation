#!/bin/bash
# File              : deploy.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 15.10.2020
# Last Modified Date: 24.11.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
source ../../configs.env
build_only=${1:-0}

if [[ "$build_only" != 0 ]]; then
	exit 0
fi

IMG=$PYTHON_IMG
file='pyperf-nbody.py'

echo "wsk -i action update ${file%.*}-python $file $CONFIGS --docker $IMG_REPO/$IMG"
wsk -i action update ${file%.*}-python $file $CONFIGS --docker $IMG_REPO/$IMG

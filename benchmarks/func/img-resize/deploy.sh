#!/bin/bash
# File              : deploy.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 16.10.2020
# Last Modified Date: 10.11.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
source ../../configs.env
build_only=${1:-0}

if [[ "$build_only" != 0 ]]; then
	exit 0
fi
rm action.zip
npm install node-zip jimp --save
zip -r action.zip ./*
#wsk action update img-resize-node --kind nodejs:10 action.zip -i
echo "wsk -i action update img-resize-node action.zip $CONFIGS --docker $IMG_REPO/$NODE_IMG"
wsk -i action update img-resize-node action.zip $CONFIGS --docker $IMG_REPO/$NODE_IMG

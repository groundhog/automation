import os
from os.path import exists

from pyperformance.benchmarks.bm_pyflate import bench_pyflake


def main(params):
    #file_path = "/usr/local/lib/python3.7/dist-packages/pyperformance/benchmarks/data/interpreter.tar.bz2"
    file_path = os.path.dirname(os.__file__) +"/site-packages/pyperformance/benchmarks/data/interpreter.tar.bz2"
    bench_pyflake(1, file_path)




    return {}

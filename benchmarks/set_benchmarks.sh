source configs.env
source cmd.sh

user="ubuntu"
benchmarks_dir="/local/workspace/automation/benchmarks"
#  do_cmd
#  do_bg=0 ## default value
#  ruser=$1
#  host=$2
#  cmd=$3
#  do_bg=$4
#  out_null=$5

echo "Update benchmarks names"

(do_cmd $user $CORE_MACHINE "sed -i \"s/benchmarks=(.*/benchmarks=('$1')/g\" /local/workspace/automation/benchmarks/deploy_benchmarks.sh")&
(do_cmd $user $INVOKER_MACHINE "sed -i \"s/benchmarks=(.*/benchmarks=('$1')/g\" /local/workspace/automation/benchmarks/deploy_benchmarks.sh")&

wait $(jobs -p)

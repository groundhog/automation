group_id=$1
az=${2:-"Nova"}
img=${3:-"groundhog-ow-ubuntu20"}
./create_instance.sh "gh.core" $img "net$group_id" "ow-core-$group_id" $az
./create_instance.sh "gh.invoker" $img "net$group_id" "ow-invoker0-$group_id" $az

./assign_fip.sh "ow-core-$group_id" $group_id

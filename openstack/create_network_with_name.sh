id=$1
openstack network create net$id
openstack subnet create net$id-sn --network net$id --subnet-range 192.168.$id.0/24
openstack router add subnet router1 net$id-sn

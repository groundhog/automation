openstack flavor create --id 10 --vcpus 4 --ram 65536 --disk 100 gh.core
openstack flavor create --id 11 --vcpus 4 --ram 65536 --disk 50 gh.invoker
openstack flavor create --id 12 --vcpus 2 --ram 16384 --disk 50 gh.client
openstack flavor create --id 13 --vcpus 1 --ram 65536 --disk 50 gh.invoker-small
openstack flavor create --id 14 --vcpus 4 --ram 32786 --disk 50 gh.client-large
openstack flavor create --id 15 --vcpus 4 --ram 65536 --disk 100 gh.faasm
openstack flavor create --id 16 --vcpus 4 --ram 65536 --disk 100 gh.kdirty.L
openstack flavor create --id 17 --vcpus 1 --ram 8192 --disk 100 gh.nano
openstack flavor create --id 18 --vcpus 1 --ram 65536 --disk 100 gh.nano.highMEM
openstack flavor create --id 19 --vcpus 4 --ram 8192 --disk 100 gh.nano.highCPU

openstack flavor set gh.core --property hw:cpu_policy=dedicated --property hw:numa_nodes=1
openstack flavor set gh.invoker --property hw:cpu_policy=dedicated --property hw:numa_nodes=1
openstack flavor set gh.invoker-small --property hw:cpu_policy=dedicated --property hw:numa_nodes=1
openstack flavor set gh.client --property hw:cpu_policy=dedicated --property hw:numa_nodes=1
openstack flavor set gh.client-large --property hw:cpu_policy=dedicated --property hw:numa_nodes=1
openstack flavor set gh.faasm --property hw:cpu_policy=dedicated --property hw:numa_nodes=1
openstack flavor set gh.kdirty.L --property hw:cpu_policy=dedicated --property hw:numa_nodes=1
openstack flavor set gh.nano --property hw:cpu_policy=dedicated --property hw:numa_nodes=1
openstack flavor set gh.nano.highMEM --property hw:cpu_policy=dedicated --property hw:numa_nodes=1
openstack flavor set gh.nano.highCPU --property hw:cpu_policy=dedicated --property hw:numa_nodes=1

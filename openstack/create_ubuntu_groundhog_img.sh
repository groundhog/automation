wget -O /snap/microstack/openstack-cloud-images/groundhog-ow-ubuntu20.qcow2  https://groundhog.mpi-sws.org/downloads/groundhog-ow-ubuntu20.qcow2
microstack.openstack image create --disk-format qcow2 --container-format bare --public --file /snap/microstack/openstack-cloud-images/groundhog-ow-ubuntu20.qcow2 groundhog-ow-ubuntu2004

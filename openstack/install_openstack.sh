
if [[ "$(lsb_release -a  2> /dev/null || true)" =~ "Debian" ]];
then
    echo "Debian";
    sudo apt install snapd
    sudo snap install core
fi

# sudo snap install microstack --devmode --beta
# Download microstack v233 from:
wget https://groundhog.mpi-sws.org/downloads/microstack_233.snap
snap install --dangerous microstack_233.snap --beta --devmode

echo fs.inotify.max_queued_events=1048576 | sudo tee -a /etc/sysctl.conf
echo fs.inotify.max_user_instances=1048576 | sudo tee -a /etc/sysctl.conf
echo fs.inotify.max_user_watches=1048576 | sudo tee -a /etc/sysctl.conf
echo vm.max_map_count=262144 | sudo tee -a /etc/sysctl.conf
echo vm.swappiness=1 | sudo tee -a /etc/sysctl.conf
sudo sysctl -p

cat > /var/snap/microstack/common/etc/neutron/dhcp_agent.ini <<EOF
[DEFAULT]
interface_driver = openvswitch
dhcp_driver = neutron.agent.linux.dhcp.Dnsmasq
enable_isolated_metadata = True
dnsmasq_dns_servers = 8.8.8.8,8.8.4.4
dnsmasq_local_resolv = True
EOF


cat > /var/snap/microstack/common//etc/neutron/plugins/ml2/ml2_conf.ini <<EOF
[ml2]
mechanism_drivers = ovn
extension_drivers = port_security,qos,dns_domain_ports,dnsmasq_dns_servers
tenant_network_types = geneve
overlay_ip_version = 4
external_network_type = flat

[ml2_type_geneve]
vni_ranges = 1:65535
max_header_size = 38

[ml2_type_flat]
flat_networks = *

[ovn]
# TODO(dmitriis): replace the common path with a template.
ovn_nb_connection = unix:/var/snap/microstack/common/run/ovn/ovnnb_db.sock
ovn_sb_connection = unix:/var/snap/microstack/common/run/ovn/ovnsb_db.sock
EOF

sudo sysctl net.ipv4.ip_forward=1

for i in `seq 0 31`; do echo performance > /sys/devices/system/cpu/cpu$i/cpufreq/scaling_governor; done

if [[ "$(lsb_release -a  2> /dev/null || true)" =~ "Debian" ]];
then
    echo "Debian mounting";
    umount /local
    mount /dev/md2 /var/snap/microstack/common/lib/instances
fi

sed -i 's/virt_type = qemu/virt_type=kvm/g'  /var/snap/microstack/common/etc/nova/nova.conf.d/nova-snap.conf
sed -i 's/cpu_mode = host-model/cpu_mode=host-passthrough/g'  /var/snap/microstack/common/etc/nova/nova.conf.d/nova-snap.conf
sudo sed -i 's/types_hash_max_size 2048;/types_hash_max_size 2048;\n\t\t\t\tclient_max_body_size 32768M;/g' /var/snap/microstack/common/etc/nginx/snap/nginx.conf
sudo sed -i 's/client_max_body_size 0;//g' /var/snap/microstack/common/etc/nginx/snap/nginx.conf

sudo snap alias microstack.openstack openstack
sudo snap restart microstack

echo "Run on the controller (first time): $ sudo microstack init --auto --control"
echo "For further hosts:"
echo "  > Run on the controller: $ sudo microstack add-compute"
echo "  > Run on the compute machine: $ sudo microstack init --auto --compute --join <connection-string>"

echo "[IMPORTANT] Test OpenStack deployment: $ microstack launch cirros --name test"

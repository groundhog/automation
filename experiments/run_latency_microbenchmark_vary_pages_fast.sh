#!/bin/bash
source cmd.sh

exptime=$(date +"%Y_%m_%d-%H_%M_%S")
experiment_dir="/local/workspace/automation/benchmarks/benchmarks/$exptime-latency-c-micro-vm-size"
ip=$(hostname -I | cut -d" " -f1)
username=$(echo ${USER})
echo $username @ $ip

mkdir -p $experiment_dir
cpus_per_container=1
num_containers=1
# Customizations to run on multiple machines with sequential IPs
# can be made by expanding the lists below.
# Most importantly, all lines of a benchmark must be run on
# the same machine (to reduce hardware variability)
#
# counts control the number of experiment repetitions
configs=('--cold --warm --thpt --warm_count 100')
lines=('baseline gh-nop forkline groundhog-sd')
instrument=('0')
counts=('1')
benchmarks=('memtest-dirtying-c')

start_ip=201

nmaps=100
dirty=1000
for i in `seq 0 0`;
do
	(
	./keep_pinging_till_up.sh 10.20.20.$(($start_ip+$i))
	echo "ubuntu@10.20.20.$(($start_ip+$i)) cd /local/workspace/automation/benchmarks &&  git checkout . && git pull && ./build-openwhisk.sh $cpus_per_container && ./deploy_openwhisk.sh 0 0 $num_containers"
	do_cmd "ubuntu" 10.20.20.$(($start_ip+$i)) "cd /local/workspace/automation/benchmarks &&  git checkout . && git pull && ./build-openwhisk.sh $cpus_per_container && ./deploy_openwhisk.sh 0 0 $num_containers"
	for c in `seq 0 0`;
	do
			run_dir=$experiment_dir/$exptime-dirty_$dirty-nmaps_$nmaps-varying-npages-inst_${instrument[i]}-config_$( echo "${configs[i]}" | sed 's#-##g' | sed 's# #-#g')
			mkdir -p $run_dir

		echo "ubuntu@10.20.20.$(($start_ip+$i)) cd workspace/automation/benchmarks && ./run_experiments.sh \"${lines[i]}\" ${counts[i]} ${instrument[i]} \"${configs[i]} --micro_dirty 100 10 5 2 1 --micro_npages 10 100 200 500 1000 --micro_nmaps 100 100 100 100 100\" \"${benchmarks[i]}\" $cpus_per_container $num_containers && ./copy_data_to_controller.sh $username $ip $run_dir"
		do_cmd "ubuntu" 10.20.20.$(($start_ip+$i)) "cd /local/workspace/automation/benchmarks && ./run_experiments.sh \"${lines[i]}\" ${counts[i]} ${instrument[i]} \"${configs[i]} --micro_dirty 100 10 5 2 1 --micro_npages 10 100 200 500 1000 --micro_nmaps 100 100 100 100 100\" \"${benchmarks[i]}\" $cpus_per_container $num_containers && ./copy_data_to_controller.sh $username $ip $run_dir"
	done
	)&
done
wait $(jobs -p)

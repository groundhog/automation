#!/bin/bash
source cmd.sh

exptime=$(date +"%Y_%m_%d-%H_%M_%S")
experiment_dir="/local/workspace/automation/benchmarks/benchmarks/$exptime-xput-NodeJS"
ip=$(hostname -I | cut -d" " -f1)
username=$(echo ${USER})
echo $username @ $ip

mkdir -p $experiment_dir
cpus_per_container=1
num_containers=('4')
vm_cpus=('4')
# Customizations to run on multiple machines with sequential IPs
# can be made by expanding the lists below.
# Most importantly, all lines of a benchmark must be run on
# the same machine (to reduce hardware variability)
#
# counts control the number of experiment repetitions
configs=('--thpt --thpt_duration 150 --scale_out')
lines=('baseline bl-refactored gh-nop groundhog-sd')

instrument=('0')
counts=('1')
benchmarks=('get-time-node autocomplete-node json-node primes-node img-resize-node base64-node ocr-img-node')
scale_factor=('4')
start_ip=201
for i in `seq 0 0`;
do
	(
	./keep_pinging_till_up.sh 10.20.20.$(($start_ip+$i))
	for c in `seq 0 0`;
	do
		echo "ubuntu@10.20.20.$(($start_ip+$i)) cd /local/workspace/automation/benchmarks &&  git checkout . && git pull && ./build-openwhisk.sh $cpus_per_container ${vm_cpus[i]} && ./deploy_openwhisk.sh 0 0 ${num_containers[i]}"
		do_cmd "ubuntu" 10.20.20.$(($start_ip+$i)) "cd /local/workspace/automation/benchmarks &&  git checkout . && git pull && ./build-openwhisk.sh $cpus_per_container ${vm_cpus[i]} && ./deploy_openwhisk.sh 0 0 ${num_containers[i]}"

		run_dir=$experiment_dir/$exptime-containers_${num_containers[i]}-vmCPUs_${vm_cpus[i]}-config_$( echo "${configs[i]}" | sed 's#-##g' | sed 's# #-#g')
		mkdir -p $run_dir
	echo "ubuntu@10.20.20.$(($start_ip+$i)) cd workspace/automation/benchmarks && ./run_experiments.sh \"${lines[i]}\" ${counts[i]} ${instrument[i]} \"${configs[i]} --scale_factor ${scale_factor[i]}\" \"${benchmarks[i]}\" $cpus_per_container ${num_containers[i]} && ./copy_data_to_controller.sh $username $ip $run_dir"
	do_cmd "ubuntu" 10.20.20.$(($start_ip+$i)) "cd /local/workspace/automation/benchmarks && ./run_experiments.sh \"${lines[i]}\" ${counts[i]} ${instrument[i]} \"${configs[i]} --scale_factor ${scale_factor[i]}\" \"${benchmarks[i]}\" $cpus_per_container ${num_containers[i]} && ./copy_data_to_controller.sh $username $ip $run_dir"
	done
	)&
done
wait $(jobs -p)

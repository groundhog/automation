source cmd.sh

gid=$1
gcloud compute instances start ow-core-$gid  --zone us-central1-c
gcloud compute instances start ow-invoker0-$gid --zone us-central1-c
gcloud compute instances start ow-client-$gid --zone us-central1-c

echo "Waiting for machines ow-core/invoker0/client-$gid to be up ..."
sleep 90

./keep_pinging_till_up.sh ow-core-$gid

echo "Setting permissions to start containers ..."

do_cmd "ubuntu" ow-core-$gid "sudo chown ubuntu:ubuntu /var/run/docker.sock"
do_cmd "ubuntu" ow-invoker0-$gid "sudo chown ubuntu:ubuntu /var/run/docker.sock"
do_cmd "ubuntu" ow-client-$gid "sudo chown ubuntu:ubuntu /var/run/docker.sock"

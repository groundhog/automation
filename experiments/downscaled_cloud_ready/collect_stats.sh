#!/bin/bash
source cmd.sh
source configs.env

for i in `seq 201 204`;
do
	latest_dir=$(do_cmd "ubuntu" 10.20.20.$i "ls -td /local/workspace/automation/benchmarks/benchmarks/* | head -1" )
	echo $latest_dir
	rsync -e "ssh $SSH_EXTRA_ARGS" -azP ubuntu@10.20.20.$i:$latest_dir $latest_dir-vm-$i
done

#!/bin/bash
# File              : cmd.sh
# Author            : Mohamed Alzayat <alzayat@mpi-sws.org>
# Date              : 04.06.2020
# Last Modified Date: 06.12.2020
# Last Modified By  : Mohamed Alzayat <alzayat@mpi-sws.org>
source configs.env

function do_cmd() {
  do_bg=0 ## default value
  ruser=$1
  host=$2
  cmd=$3
  do_bg=$4
  out_null=$5

#  echo "1. ssh $ruser@$host \"$cmd\" $do_bg $out_null"
  if [[ $dryrun -eq 0 ]]; then
    if [[ $do_bg -eq 1 ]]; then
      if [[ $out_null -eq 1 ]]; then
        #echo "ssh -f $ruser@$host \"$cmd &\" > /dev/null 2>&1"
        ssh $SSH_EXTRA_ARGS -f $ruser@$host "$cmd &" > /dev/null 2>&1
      else
        #echo "ssh -f $ruser@$host \"$cmd\""
        ssh $SSH_EXTRA_ARGS  -f $ruser@$host "$cmd" #> /dev/null 2>&1
      fi
    else
      if [[ $out_null -eq 1 ]]; then
        ssh $SSH_EXTRA_ARGS $ruser@$host "$cmd" > /dev/null 2>&1
      else
        ssh $SSH_EXTRA_ARGS $ruser@$host "$cmd"
      fi
    fi
  fi
}

function start_mpstat()
{
    ruser=$1
    host=$2
		file=$3
    cmd="mpstat -u -P ALL 1 > $file-mpstats.out"
		cmd2="cd /local/workspace/automation/benchmarks && ./start_docker_stats.sh > $file-docker_stat.out"
		do_cmd $ruser $host "$cmd" 1 1
    do_cmd $ruser $host "$cmd2" 1 1
}

function stop_mpstat()
{
    ruser=$1
    host=$2
    cmd="pkill -9 mpstat"
		cmd2="cd /local/workspace/automation/benchmarks && ./kill_docker_stats.sh"
    do_cmd $ruser $host "$cmd" 1 1
    do_cmd $ruser $host "$cmd2" 1 1
}


function get_gitinfo()
{
  host=$1
  dir=$2
	outdir=$3
  suffix=$4
  cmd="cd $dir; git show --name-status > $outdir/gitinfo_$suffix; "
  cmd=$cmd"echo "" >> $outdir/gitinfo_$suffix; "
  cmd=$cmd"git diff >> $outdir/gitinfo_$suffix; "
	do_cmd $user $host "$cmd"
}


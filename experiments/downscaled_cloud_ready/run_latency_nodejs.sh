#!/bin/bash
source cmd.sh

exptime=$(date +"%Y_%m_%d-%H_%M_%S")
experiment_dir="/local/workspace/automation/benchmarks/benchmarks/$exptime-latency-NodeJS"
ip=$(hostname -I | cut -d" " -f1)
username=$(echo ${USER})
echo $username @ $ip

mkdir -p $experiment_dir
cpus_per_container=1
num_containers=1

# Customizations to run on multiple machines with sequential IPs
# can be made by expanding the lists below.
# Most importantly, all lines of a benchmark must be run on
# the same machine (to reduce hardware variability)
#
# counts control the number of experiment repetitions

configs=('--pause 200 --cold --cold_count 1 --warm --warm_count 50')
lines=('baseline baseline-refactored gh-nop groundhog-sd')
lines=('baseline gh-nop groundhog-sd')
instrument=('1')
counts=('1')
benchmarks=('get-time-node autocomplete-node json-node primes-node img-resize-node base64-node ocr-img-node')
core_vm_name="ow-core-1"
for i in `seq 0 0`;
do
	(
	./keep_pinging_till_up.sh $core_vm_name
	echo "ubuntu@$core_vm_name cd /local/workspace/automation/benchmarks &&  git checkout . && git pull && ./build-openwhisk.sh $cpus_per_container && ./deploy_openwhisk.sh 0 0 $num_containers"
	do_cmd "ubuntu" $core_vm_name "cd /local/workspace/automation/benchmarks &&  git checkout . && git pull && ./build-openwhisk.sh $cpus_per_container && ./deploy_openwhisk.sh 0 0 $num_containers"
	for c in `seq 0 0`;
	do
		run_dir=$experiment_dir/$exptime-inst_${instrument[i]}-config_$( echo "${configs[i]}" | sed 's#-##g' | sed 's# #-#g')
		mkdir -p $run_dir:
	  echo "ubuntu@$core_vm_name cd workspace/automation/benchmarks && ./run_experiments.sh \"${lines[i]}\" ${counts[i]} ${instrument[i]} \"${configs[i]}\" \"${benchmarks[i]}\" $cpus_per_container $num_containers && ./copy_data_to_controller.sh $username $ip $run_dir"
	  do_cmd "ubuntu" $core_vm_name "cd /local/workspace/automation/benchmarks && ./run_experiments.sh \"${lines[i]}\" ${counts[i]} ${instrument[i]} \"${configs[i]}\" \"${benchmarks[i]}\" $cpus_per_container $num_containers && ./copy_data_to_controller.sh $username $ip $run_dir"
	done
	)&
done
wait $(jobs -p)
